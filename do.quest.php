<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');

if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}
$fields = array(
	'STRING' => array(
		'title',
		'text',
		'summary',
			
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
		'date',
	),
	'DATETIME' => array(
	),
);
$mainTable = 'quizes';

$nextPage = 'index.php?c=question_management';

switch ($op) {
      case 'I': // Inserimento
      
			if ($id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError)) {
		
				if ($_FILES['image']['name']) {
					require('do.news.saveImage.php');
					
				}
				if ($_FILES['attachment']['name']) {
					require('do.news.saveAttach.php');
					
				}
			}
			break;
   case "U": // Aggiornamento
	    	doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
			
	    	if (!$sqlError) {
				if ($_FILES['image']['name']) {
					require('do.news.removeImage.php');
					require('do.news.saveImage.php');
				}
				if ($_FILES['attachment']['name']) {
					require('do.news.removeAttach.php');
					require('do.news.saveAttach.php');
				}
	    	}
    	break;
	case "D": 
			echo"in";
			// delete the image
			
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
			
			//delete question image
			if (file_exists("images/questions_and_solutions/question".$id.".jpg")) {
				unlink("images/questions_and_solutions/question".$id.".jpg");
			}
			
			$sql = 'SELECT answer FROM answers WHERE question='.$id;
			$images = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			foreach ($images as $field => $value) {

				if (file_exists("images/questions_and_solutions/".$value['answer'])) {
							unlink("images/questions_and_solutions/".$value['answer']);
						}
			}
			
			
			
			$sql = 'DELETE FROM answers WHERE question='.$id;
			sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
        break;
	case 'S':
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
			
		break;
}
?>
<script language="javascript" type="text/javascript">
window.location = "<?php echo $nextPage?>";
</script>