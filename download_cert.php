<?php
require('inc.init.php');
require_once('core/inc.config.php');
require('inc.check.php');
require_once('core/func.mysqlPrepare.php');
require_once('core/func.getListOptions.php');
require_once('core/func.getSqlOptions.php');

require('libs/fpdf181/fpdf.php');

$record_id = intval($_GET['record']);
  $sql1 =	"SELECT users.name as fname, surname, DATE_FORMAT(results.insTS, '%d %M %Y') as date, Title,results.id as record
				FROM `results` inner join issue as I on I.id =results.issue_no 
				inner join _users  on _users.id = results.insu
				inner join users  on users.id = _users.userid
				WHERE  mark >= passmark AND results.id= $record_id";
        
        $record = sqlExecute($connection, $sql1, $sqlError, $sqlCount, basename(__FILE__), DEBUG);

class PDF extends FPDF
{
// Page header
/*function Header()
{
    // Logo
   // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'Title',1,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}*/
}

// Instanciation of inherited class
$pdf = new PDF();

$pdf->AliasNbPages();
$pdf->AddPage("L","A4",0);
//$pdf->Image('certificate.jpg', 0, 0, $fpdf->w, $fpdf->h);
$pdf->Image('img/certificate.jpg', 0.5, 1, 0 , 207);
$pdf->SetFont('Times','',20);
//for($i=1;$i<=40;$i++)
	//$pdf->Cell(100);
$pdf->SetXY(0, 80);
    $pdf->Cell(0,0,$record[0]['fname']." ".$record[0]['surname'],0,1,"C");

$pdf->SetXY(0, 110);
$pdf->Cell(0,0,$record[0]['Title'],0,1,"C");

$pdf->SetXY(125, 160);
$pdf->Cell(0,0,$record[0]['date'],0,1,"L");

//$pdf->SetXY(220, 160);
$pdf->Image('img/test.png',220, 155,30,10);
//$pdf->Cell(0,0,'signture',0,,"L");

$pdf->SetFont('Times','',15);
$pdf->SetXY(240, 185);
$pdf->SetTextColor(256,0,0);
$pdf->Cell(0,0,'No:'.str_pad($record[0]['record'],8,"0",STR_PAD_LEFT),0,1,"L");
$pdf->Output("D","cert-".str_pad($record[0]['record'],8,"0",STR_PAD_LEFT).".pdf",false);
?>