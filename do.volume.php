<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');

//exit;
if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}
$fields = array(
	'STRING' => array(
		'name',
		'intials',
		'surname',
		'nationalId',
		'gender',
		'address1',
		'address2',
		'address3',
		'address4',
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
		'dob',
		
	),
	'DATETIME' => array(
	),
);

$fields1 = array(
	'STRING' => array(
		
		'class',
			
	),
	'INT' => array(
	'session',
	),
	'FLOAT' => array(
	),
	'DATE' => array(
		'test_date',
	),
	'DATETIME' => array(
	),
);
$fieldsu = array(
	'STRING' => array(
	
	),
	'INT' => array(
		'clientid',
	),
	'FLOAT' => array(
	),
	'DATE' => array(
	
	),
	'DATETIME' => array(
	),
);

$fieldslog = array(
	'STRING' => array(
	'username',
	'user_password',
	'name',
	'email',
	'profile',
	'active',
	),
	'INT' => array(
		
	),
	'FLOAT' => array(
	),
	'DATE' => array(
	
	),
	'DATETIME' => array(
	),
);

$nextPage = 'index.php?c=question_management';

switch ($op) {
      case 'I': // Inserimento
      
			if ($id = doOperation($connection, 'I', 'clients', $fields, $_POST, $sqlError)) {
					$bid=doOperation($connection, 'I', 'bookings', $fields1, $_POST, $sqlError);
			
					$input= array('clientid'=>$bid);
					$input1= array('name'=>$_POST['name'],'username'=>$_POST['nationalId'],'user_password'=>'password','email'=>$_POST['email'],'profile'=>'S','active'=>0);
				
					doOperation($connection, 'U', 'bookings', $fieldsu,$input, $sqlError, $bid);
					doOperation($connection, 'I', '_users', $fieldslog, $input1, $sqlError);
				
			}
			break;
   case "U": // Aggiornamento
	    	doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
			
	    	if (!$sqlError) {
				if ($_FILES['image']['name']) {
					require('do.news.removeImage.php');
					require('do.news.saveImage.php');
				}
				if ($_FILES['attachment']['name']) {
					require('do.news.removeAttach.php');
					require('do.news.saveAttach.php');
				}
	    	}
    	break;
	case "D": 
			echo"in";
			// delete the image
			
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
			
			//delete question image
			if (file_exists("images/questions_and_solutions/question".$id.".jpg")) {
				unlink("images/questions_and_solutions/question".$id.".jpg");
			}
			
			$sql = 'SELECT answer FROM answers WHERE question='.$id;
			$images = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			foreach ($images as $field => $value) {

				if (file_exists("images/questions_and_solutions/".$value['answer'])) {
							unlink("images/questions_and_solutions/".$value['answer']);
						}
			}
			
			
			
			$sql = 'DELETE FROM answers WHERE question='.$id;
			sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
        break;
	case 'S':
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
			
		break;
}
?>

<script language="javascript" type="text/javascript">
window.location = "<?php echo $nextPage?>";
</script>