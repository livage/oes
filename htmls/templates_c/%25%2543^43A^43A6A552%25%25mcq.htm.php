<?php /* Smarty version 2.6.10, created on 2016-07-21 12:56:10
         compiled from mcq.htm */ ?>
<div class="container-fluid">
		<div class="row-fluid">

<div id="content" class="span10">

			
			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-info-sign"></i> Questions Management</h2>
						
					</div>
					<div class="box-content">

<script type="text/javascript" src="js/hide.js" ></script>
<div id="formpage_1" name="insert" style="visibility: visible; display: block; .."> 
  
 <form action="do_questions.php?type=MCQ" method="post" enctype="multipart/form-data">

	  <div id="content">
        <fieldset class="manufacturer">
		<legend>Test Question</legend>
		
		<div class="">
			 	<label>Issue :</label>
			<select name="issue"  class="form-control" required >
			<option value="" >Select an Issue</option>
			<?php unset($this->_sections['listb']);
$this->_sections['listb']['name'] = 'listb';
$this->_sections['listb']['loop'] = is_array($_loop=$this->_tpl_vars['issue']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['listb']['show'] = true;
$this->_sections['listb']['max'] = $this->_sections['listb']['loop'];
$this->_sections['listb']['step'] = 1;
$this->_sections['listb']['start'] = $this->_sections['listb']['step'] > 0 ? 0 : $this->_sections['listb']['loop']-1;
if ($this->_sections['listb']['show']) {
    $this->_sections['listb']['total'] = $this->_sections['listb']['loop'];
    if ($this->_sections['listb']['total'] == 0)
        $this->_sections['listb']['show'] = false;
} else
    $this->_sections['listb']['total'] = 0;
if ($this->_sections['listb']['show']):

            for ($this->_sections['listb']['index'] = $this->_sections['listb']['start'], $this->_sections['listb']['iteration'] = 1;
                 $this->_sections['listb']['iteration'] <= $this->_sections['listb']['total'];
                 $this->_sections['listb']['index'] += $this->_sections['listb']['step'], $this->_sections['listb']['iteration']++):
$this->_sections['listb']['rownum'] = $this->_sections['listb']['iteration'];
$this->_sections['listb']['index_prev'] = $this->_sections['listb']['index'] - $this->_sections['listb']['step'];
$this->_sections['listb']['index_next'] = $this->_sections['listb']['index'] + $this->_sections['listb']['step'];
$this->_sections['listb']['first']      = ($this->_sections['listb']['iteration'] == 1);
$this->_sections['listb']['last']       = ($this->_sections['listb']['iteration'] == $this->_sections['listb']['total']);
?>
	<option value="<?php echo $this->_tpl_vars['issue'][$this->_sections['listb']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['issue'][$this->_sections['listb']['index']]['Title']; ?>
 (<?php echo $this->_tpl_vars['issue'][$this->_sections['listb']['index']]['name']; ?>
)</option>
	<?php endfor; endif; ?>
			</select>
			 </div>
	        <br style="clear:both" />
			<div class="boxfield">
			 	<label>Question:</label>
			 <input type="text" name="question" size="70" class="form-control" required> 
			 
			 </div>
		
			
			
			 <br style="clear:both" />
			 
			  <div id="textsol"  class="boxfield">
			 <label> Correct Answer:</label> <input type="text" name="correctAns" size='70' class="form-control" required />
			  </div>
			  <br>
		
			
			
			  <div class="boxfield">
			 
			 <div id="misleading">
			<b>	Misleading Solutions:</b>
			 	
				 <br style="clear:both" />
			 <div id="textmis"  class="boxfield">
			 <label>Answer :</label><input type="text" name="answer[]" size='70' class="form-control" required  > 
			 </div><br>
			
			  </div>
			 </div>
			 <br style="clear:both" />
			 <div class="boxfield">
			 <input type="button" value="Add" class="btn btn-special btn-color pull-right" onClick="return newupload()"> 
			 </div>
			 
        </fieldset>
		<br>
	    <input type="submit" value="Confirm" class="btn btn-special btn-color "/>
	    <input type="button" value="Cancel" class="btn btn-special btn-color " href="question_list.php" />
	 </div>
</form>

</div>
</div>
				</div>
			</div>
</div>
<!--<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>-->
</div>
<!--<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>-->
</div>