<?php /* Smarty version 2.6.10, created on 2018-04-05 02:51:32
         compiled from ce.htm */ ?>
<fieldset>
         <h3 class="title">CE Tracker</h3>
                        <table width="400px" class="table table-striped table-bordered" align="right">
                           <thead>
                              <tr>
                                 <th>Issue Title</th>
                                 <th>Date Taken</th>
                                 <th>Score</th>
                                 <th>Certificate</th>
                              </tr>
                           </thead><tbody>
						     <?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['passes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
							<tr>
								<td><?php echo $this->_tpl_vars['passes'][$this->_sections['list']['index']]['Title']; ?>
</td>
								<td class="center"><?php echo $this->_tpl_vars['passes'][$this->_sections['list']['index']]['insTS']; ?>
</td>
								
								<td class="center">
									<?php echo $this->_tpl_vars['passes'][$this->_sections['list']['index']]['mark']; ?>

								</td>
								<td class="center">
									
									<a class="btn btn-danger" href="download_cert.php?record=<?php echo $this->_tpl_vars['passes'][$this->_sections['list']['index']]['record']; ?>
">
										<i class="icon-trash icon-white"></i> 
										Download
									</a>
								</td>
							</tr>
							
							<?php endfor; endif; ?>
						   
						   
                              
                           </tbody>
                        </table>
    <p>
      <strong>
        Your current CE score is.
      </strong>
    </p>
    <p>
      <?php echo $this->_tpl_vars['score']; ?>

    </p>
    <p>
     </fieldset>