<?php /* Smarty version 2.6.10, created on 2016-06-14 13:14:53
         compiled from front.htm */ ?>
<!-- logon register form -->
<form id="fapNetui_Form_0" action="#" class="transactionForm" method="post">

    
  <fieldset>
    <p>
      <strong>
        During your transaction:
      </strong>
    </p>
    <p>
      <ul>
        <li>
          Please don't use the buttons on your browser (e.g. 'back', 'forward', 'refresh' etc.), you should only use the buttons on the bottom of each page and you should click these only once.
        </li>
        <li>
          Please have all your documents to hand as the service will timeout when not in use.
        </li>
        <li>
          Please ensure the details you enter are your own. You cannot apply for a driving licence for someone else.
        </li>
      </ul>
    </p>
    <p>
	<?php if ($this->_tpl_vars['payments'] > 0): ?>
     <a href="index.php?c=paper&isu=<?php echo $_GET['isu']; ?>
">Start</a>

<?php else: ?>
<a href="paynow/do.payment.php?client=<?php echo $_SESSION['pcd']['login_data']['id']; ?>
&isu=<?php echo $_GET['isu']; ?>
"  target="_blank">Make Payment</a>

<?php endif; ?>
</form>