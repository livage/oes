<?php /* Smarty version 2.6.10, created on 2016-09-14 12:22:18
         compiled from box/pagination.htm */ ?>
<div class="pagination">
	<div class="text">Results: <strong><?php echo $this->_tpl_vars['results']; ?>
</strong> </div>
	<?php if ($this->_tpl_vars['pages'] > 1): ?>
		<div class="paginationLeftBorder"></div>
		<div class="text"> Pages: </div>
	    <?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['pageRange']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['step'] = 1;
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = $this->_sections['index']['loop'];
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
			<?php if ($this->_tpl_vars['pageRange'][$this->_sections['index']['index']] == $this->_tpl_vars['page']): ?>
			<div class="thisPage"><?php echo $this->_tpl_vars['pageRange'][$this->_sections['index']['index']]; ?>
</a>
			<?php else: ?>
			<a href="index.php?c=<?php echo $this->_tpl_vars['_ENGINE']['content']; ?>
&pg=<?php echo $this->_tpl_vars['pageRange'][$this->_sections['index']['index']]; ?>
" class="otherPages"><?php echo $this->_tpl_vars['pageRange'][$this->_sections['index']['index']]; ?>
</a>
			<?php endif; ?>
	    <?php endfor; endif; ?>
		<div class="paginationRightBorder"></div>
	<?php endif; ?>
</div>