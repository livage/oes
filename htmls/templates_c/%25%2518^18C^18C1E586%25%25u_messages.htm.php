<?php /* Smarty version 2.6.10, created on 2011-01-20 15:41:38
         compiled from u_messages.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'u_messages.htm', 35, false),)), $this); ?>
<form action="" method="post">
	<input class="search_input" style="width: 625px; height: 30px"type="text" name="search" value="Whats on your mind?" />
</form>
<?php if ($this->_tpl_vars['pastorMessage']['text']): ?>
<div class="dailyposts">Daily prophecies</div>
<div class="pastorsmessage_box">
    <img src="images/ProphetTB_Joshua.jpg" class="pastormain" />
	<div class="message_boxCon">
		<div class="pastorTitle">
			<img src="images/pastorslogo.png" class="pastorlogo" />
			<div class="pastorName">Prophet TB Joshua</div>
			<div class="pastorTitle"><?php echo $this->_tpl_vars['pastorMessage']['title']; ?>
</div>
			<div class="date"><?php echo $this->_tpl_vars['pastorMessage']['dateDMYHM']; ?>
</div>
        </div>
        <div class="message_text">
        	<?php echo $this->_tpl_vars['pastorMessage']['text']; ?>

  		</div>
  	</div>
</div>
<div class="break"></div>
<div onclick="$('#newComment').slideDown(400)" class="newComment"><img src="images/icons/comment.png" /> <div>Post your Comment</div></div>
<div class="postCon_box newCommentBox" id="newComment" style="display:none;">
	<div class="commentTitle">Enter your coment to the pastor Daily Post</div>
	<textarea name="message"></textarea>
	<input type="hidden" name="post" value="<?php echo $this->_tpl_vars['pastorMessage']['id']; ?>
" />
	<div class="sendMessage" onclick="postComment($('#newComment'))">Send your Comment</div>
</div>
<div class="postCon_box">
	<?php unset($this->_sections['comment']);
$this->_sections['comment']['name'] = 'comment';
$this->_sections['comment']['loop'] = is_array($_loop=$this->_tpl_vars['userMessages']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['comment']['show'] = true;
$this->_sections['comment']['max'] = $this->_sections['comment']['loop'];
$this->_sections['comment']['step'] = 1;
$this->_sections['comment']['start'] = $this->_sections['comment']['step'] > 0 ? 0 : $this->_sections['comment']['loop']-1;
if ($this->_sections['comment']['show']) {
    $this->_sections['comment']['total'] = $this->_sections['comment']['loop'];
    if ($this->_sections['comment']['total'] == 0)
        $this->_sections['comment']['show'] = false;
} else
    $this->_sections['comment']['total'] = 0;
if ($this->_sections['comment']['show']):

            for ($this->_sections['comment']['index'] = $this->_sections['comment']['start'], $this->_sections['comment']['iteration'] = 1;
                 $this->_sections['comment']['iteration'] <= $this->_sections['comment']['total'];
                 $this->_sections['comment']['index'] += $this->_sections['comment']['step'], $this->_sections['comment']['iteration']++):
$this->_sections['comment']['rownum'] = $this->_sections['comment']['iteration'];
$this->_sections['comment']['index_prev'] = $this->_sections['comment']['index'] - $this->_sections['comment']['step'];
$this->_sections['comment']['index_next'] = $this->_sections['comment']['index'] + $this->_sections['comment']['step'];
$this->_sections['comment']['first']      = ($this->_sections['comment']['iteration'] == 1);
$this->_sections['comment']['last']       = ($this->_sections['comment']['iteration'] == $this->_sections['comment']['total']);
?>
	<div id="commentsSet">
		<div class="postmessage_boxCon">
			<div class="commentItem">
				<div class="userImg">
					<img src="<?php echo ((is_array($_tmp=@$this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['picture'])) ? $this->_run_mod_handler('default', true, $_tmp, 'images/pic.png') : smarty_modifier_default($_tmp, 'images/pic.png')); ?>
" class="postimg" />
				</div>
				<div class="centralBox">
				    <div class="title">
				    	<div class="name">
				    		<strong><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['name']; ?>
 <?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['surname']; ?>
</strong>
				    		- Writing from <strong><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['city']; ?>
</strong><br />
				    		<div class="dataora datecoment"><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['datetime']; ?>
</div>
				    	</div>
				    	<!--<div class="dataora"><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['datetime']; ?>
</div>-->
				    </div>
				    <div class="message_text"><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['message']; ?>
</div>
				</div>
				<div class="countryBox">
			    	<div>
			    		<img src="images/countries/<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['country']; ?>
_mini.png" />
		    			<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['countryName']; ?>

		    		</div>
		    		<img src="images/countries/<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['country']; ?>
_location.png" class="userLocation" />
			    </div>
				<div class="linkSet">
			    	<a href="#" class="numberofcomments">2 Comments</a>
				    <a href="#" class="comment">Comment</a>
				    <a href="#" class="sendemail">Send email of blessing</a>
			    </div>
			</div>
		</div>
				<div class="repliesSet">
		<?php unset($this->_sections['reply']);
$this->_sections['reply']['name'] = 'reply';
$this->_sections['reply']['loop'] = is_array($_loop=$this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['replies']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['reply']['show'] = true;
$this->_sections['reply']['max'] = $this->_sections['reply']['loop'];
$this->_sections['reply']['step'] = 1;
$this->_sections['reply']['start'] = $this->_sections['reply']['step'] > 0 ? 0 : $this->_sections['reply']['loop']-1;
if ($this->_sections['reply']['show']) {
    $this->_sections['reply']['total'] = $this->_sections['reply']['loop'];
    if ($this->_sections['reply']['total'] == 0)
        $this->_sections['reply']['show'] = false;
} else
    $this->_sections['reply']['total'] = 0;
if ($this->_sections['reply']['show']):

            for ($this->_sections['reply']['index'] = $this->_sections['reply']['start'], $this->_sections['reply']['iteration'] = 1;
                 $this->_sections['reply']['iteration'] <= $this->_sections['reply']['total'];
                 $this->_sections['reply']['index'] += $this->_sections['reply']['step'], $this->_sections['reply']['iteration']++):
$this->_sections['reply']['rownum'] = $this->_sections['reply']['iteration'];
$this->_sections['reply']['index_prev'] = $this->_sections['reply']['index'] - $this->_sections['reply']['step'];
$this->_sections['reply']['index_next'] = $this->_sections['reply']['index'] + $this->_sections['reply']['step'];
$this->_sections['reply']['first']      = ($this->_sections['reply']['iteration'] == 1);
$this->_sections['reply']['last']       = ($this->_sections['reply']['iteration'] == $this->_sections['reply']['total']);
?>
			<?php if ($this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['replies'][$this->_sections['reply']['index']]['id']): ?>
			<div class="commentCon_box">
				<div class="dataora datecoment"><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['replies'][$this->_sections['reply']['index']]['datetime']; ?>
</div>
					<img src="<?php echo ((is_array($_tmp=@$this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['replies'][$this->_sections['reply']['index']]['picture'])) ? $this->_run_mod_handler('default', true, $_tmp, 'images/pic.png') : smarty_modifier_default($_tmp, 'images/pic.png')); ?>
" class="postimg profileimg" />
					<div class="comment_boxCon">
					<div class="title">
						<div class="name"><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['replies'][$this->_sections['reply']['index']]['name']; ?>
 <?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['replies'][$this->_sections['reply']['index']]['surname']; ?>
</div>
					</div>
					<div class="message_text"><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['replies'][$this->_sections['reply']['index']]['message']; ?>
</div>
					<a href="#" class="numberofcomments">2 Comments</a>
					<a href="#" class="comment">Comment</a>
					<a href="#" class="sendemail">Send email of blessing</a>
				</div>
			</div>
			<?php endif; ?>
					<?php endfor; endif; ?>
		</div>
		<div class="break"></div>
		<div onclick="$('#newReply_<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['id']; ?>
').slideDown(400)" class="newReply"><img src="images/icons/reply.png" /> <div>Reply to <?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['name']; ?>
 <?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['surname']; ?>
</div></div>
		<div class="commentCon_box newReplyBox" id="newReply_<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['id']; ?>
" style="display:none;">
			<div class="replyTitle">Enter you reply to <strong><?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['name']; ?>
 <?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['surname']; ?>
</strong></div>
			<textarea name="message"></textarea>
			<input type="hidden" name="post" value="<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['post']; ?>
" />
			<input type="hidden" name="comment" value="<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['id']; ?>
" />
			<div class="sendMessage" onclick="postReply($('#newReply_<?php echo $this->_tpl_vars['userMessages'][$this->_sections['comment']['index']]['id']; ?>
'))">Send your Reply</div>
		</div>
	</div>
	<?php endfor; endif; ?>
</div>
<?php endif; ?>
