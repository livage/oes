<?php /* Smarty version 2.6.10, created on 2017-04-12 10:48:52
         compiled from about.htm */ ?>
<!-- Content Start -->
         <div id="main">
            <!-- Title, Breadcrumb Start-->
            <div class="breadcrumb-wrapper">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                       
                     </div>
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <div class="breadcrumbs pull-right">
                           <ul>
                              <li>You are here:</li>
                             
                              <li>About Us</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Title, Breadcrumb End-->
            <!-- Main Content start-->
            <div class="content">
               <div class="container">
                  <div class="row">
                     <div class="posts-block col-lg-8 col-md-8 col-sm-6 col-xs-12">
                        <article>
                           <h3 class="title">Who We Are?</h3>
                           <div class="post-content">
                              
                              <p align="justify">
                                 The Drug and Toxicology Information Service (DaTIS) is a unit within the School of Pharmacy at the University of Zimbabwe, College of Health Sciences which is also supported by the Ministry of Health and Child Welfare as a mission set up in 1979. The unit is also recognized by the World Health Organisation as a poisons information centre and is listed as such on the WHO website. DaTIS has traditionally provided consultation to mostly health care professionals and, to a smaller extent, the general public on drug information and toxicological/poisoning related issues.
                              </p>
                              <br>
                              <p align="justify">This service is provided nationally largely through a 24-hour telephone service where hospitals and clinics receive the numbers of the pharmacists/toxicologists on call after hours. During working hours (0800-1630), callers are given an immediate response whenever possible or by phone call returned as soon as possible. The unit also offers clinical pharmacokinetics consultancy services through its therapeutic drug monitoring (TDM) unit based at Parirenyatwa hospital.
                              </p>
                           </div>
                        </article>
                     </div>
                     <!-- Left Section End -->
                     <!-- Skill Section Start -->
                     <div class="skills col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <h3 class="title">Our Services</h3>
                        <div class="progress">
                           <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">Drug information</div>
                        </div>
                        <div class="progress">
                           <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">Training of pre-registration pharmacists</div>
                        </div>
                        <div class="progress">
                           <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">Expert advice on applied toxicology</div>
                        </div>
                        <div class="progress">
                           <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">Experimental toxicology research and consultancy</div>
                        </div>
                        <div class="progress">
                           <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">24-hour national poisons information service</div>
                        </div>
                     </div>
                     <!-- Skill Section Start -->
                     <div class="clearfix"></div>
                  </div>
                  
                  <div class="row">
                     
                     <div class="clearfix"></div>
                  </div>
                  <div class="divider"></div>
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <!-- Tab Start -->
                        <h3 class="title">Organisational Structure</h3>
                        <div class="widget tabs animate_afc">
                           <div id="horizontal-tabs">
                              <ul class="tabs">
                                 <li id="tab1" class="current">The DaTIS Toxicology Unit (DTU)</li>
                                 <li id="tab2">DPU</li>
                                 <li id="tab3">TDU</li>
                              </ul>
                              <div class="contents">
                                 <div class="tabscontent" id="content1" style="display: block;">
                                   <p align="justify">
                                      The mission of this arm of DaTIS is to lead teaching, research and consultancy in applied toxicology including toxicovigilance and toxicoepidemiology. This unit is run by pharmacists with extensive training and/or experience in applied toxicology.
                                    </p>
                                 </div>
                                 <div class="tabscontent" id="content2">
                                   <p align="justify"><strong>The DaTIS Pharmacotherapeutics Unit</strong><br>
                                       The mission of this unit is to lead teaching, research and consultancy in clinical pharmacy (with emphasis on drug information) and pharmacotherapeutics. This unit is run by clinical pharmacists and aims to be a regional leader in the discipline of clinical pharmacy. 
                                   </p>
                                 </div>
                                 <div class="tabscontent" id="content3">
                                   <p align="justify"><strong>The DaTIS Therapeutic Drug Monitoring Unit</strong><br>
                                      The mission of this arm of DaTIS is to lead in teaching, research and consultancy in the area of therapeutic drug monitoring and analytical toxicology, including interpretation of laboratory results offered in conjunction with the DTU and the DPU.
                                   </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Tab End -->
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <!-- Testimonials Widget Start -->
                        <div class="row">
                           <div class="testimonials widget">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="testimonials-title">
                                    <h3 class="title">Vision, Mission & Values</h3>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div id="testimonials-carousel" class="testimonials-carousel carousel slide animate_afc">
                                 <div class="carousel-inner">
                                    <div class="item active">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="testimonial item">
                                             <p>
                                               To become an independent, regional and internationally renowned and recognised center of excellence for the provision of clinical pharmacy and applied toxicology related teaching, research and consultancy services.
                                             </p>
                                             <div class="testimonials-arrow">
                                             </div>
                                             <div class="author">
                                                <div class="testimonial-image "><img alt="" src="img/testimonial/team-member-1.jpg"></div>
                                                <div class="testimonial-author-info">
                                                   <a href="#"><span class="color">Our Vision</span></a> 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="testimonial item">
                                             <p>
                                               
 - To facilitate and promote safe and effective use of drugs in Zimbabwe and regionally through the provision of up-to-date, accurate and timely drug and pharmacotherapeutics related information and services;<br>
- To facilitate and promote effective management of patients through the provision of consultancy services in therapeutic drug monitoring and analytical toxicology both nationally and regionally;<br>
- To facilitate and promote safe use of chemicals and effective clinical management of poisoning through leading teaching, research and consultancy in applied toxicology including toxicovigilance, toxicoepidemiology, clinical, environmental and experimental toxicology.

                                             </p>
                                             <div class="testimonials-arrow">
                                             </div>
                                             <div class="author">
                                                <div class="testimonial-image "><img alt="" src="img/testimonial/team-member-2.jpg"></div>
                                                <div class="testimonial-author-info">
                                                   <a href="#"><span class="color">Our Mission</span></a> 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="testimonial item">
                                             <p>
                                                
- Provision of professional, ethical, accurate and efficient drug and toxicology services<br>
- Integrity, responsibility, accountability and decisiveness in our commitment to excellence<br>
- Effective utilization of our resources  <br>
- Advancement of our institutional mission while supporting professional and personal growth

                                             </p>
                                             <div class="testimonials-arrow">
                                             </div>
                                             <div class="author">
                                                <div class="testimonial-image "><img alt="" src="img/testimonial/team-member-3.jpg"></div>
                                                <div class="testimonial-author-info">
                                                   <a href="#"><span class="color">Our Values</span></a> FIFO Themes
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="divider"></div>
                  <div class="row"></div>
               </div>
            </div>
            <!-- Main Content end-->
            <!-- Our Clients Start-->
            
            <!-- Our Clients End --> 
         </div>
         <!-- Content End -->