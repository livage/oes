<?php /* Smarty version 2.6.10, created on 2017-02-14 15:39:26
         compiled from home.htm */ ?>
  <!-- Content Start -->
         <div id="main">
            <!-- Slider Start-->
            <div class="fullwidthbanner-container">
               <div class="fullwidthbanner rslider">
                  <ul>
                     <!-- THE FIRST SLIDE -->
                     <li data-delay="6000" data-masterspeed="300" data-transition="fade">
                        <div class="slotholder"><img src="img/slider/nivo_slider1.jpg" data-fullwidthcentering="on" alt="Pixma"></div>
                        <div class="caption modern_big_bluebg h1 lft tp-caption start"
                           data-x="40"
                           data-y="85"
                           data-speed="700"
                           data-endspeed="800"
                           data-start="1000"
                           data-easing="easeOutQuint"
                           data-endeasing="easeOutQuint">
                           <h3>DaTIS Online Reviews</h3>
                        </div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="1400" 
                           data-speed="1050" 
                           data-y="180" 
                           data-x="40">
                           <div class="list-slide">
                              <i class="fa fa-camera slide-icon"></i>
                              <h5 class="dblue"> Providing necessary knowledge </h5>
                           </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="1800" 
                           data-speed="1200" 
                           data-y="220" 
                           data-x="40">
                           <div class="list-slide">
                              <i class="fa fa-search slide-icon"></i>
                              <h5 class="dblue"> Equiping Pharmacists </h5>
                           </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="2200" 
                           data-speed="1350" 
                           data-y="260" 
                           data-x="40">
                           <div class="list-slide">
                              <i class="fa fa-code slide-icon"></i>
                              <h5 class="dblue"> Earn more CPD Points </h5>
                           </div>
                        </div>
                        <div class="caption lfb caption_button_1 fadeout tp-caption start"
                           data-x="40"
                           data-y="330"
                           data-speed="900"
                           data-endspeed="300"
                           data-start="2700"
                           data-hoffset="-70"
                           data-easing="easeOutExpo">
                           <a class="btn-special hidden-xs btn-grey" href="#">Learn More</a>
                        </div>
                        <div class="caption lfb caption_button_2 fadeout tp-caption start"
                           data-x="210"
                           data-y="330"
                           data-speed="800"
                           data-endspeed="300"
                           data-start="2900"
                           data-hoffset="70"
                           data-easing="easeOutExpo">
                           <a class="btn-special hidden-xs btn-color" href="#">Take Test</a>
                        </div>
                     </li>
                     <!-- THE RESPONSIVE SLIDE -->
                     <li data-transition="fade" data-slotamount="1" data-masterspeed="300">
                        <img src="img/slider/slider-bg-2.jpg" data-fullwidthcentering="on" alt="">
                        <div class="caption large_text sft"
                           data-x="660"
                           data-y="54"
                           data-speed="300"
                           data-start="800"
                           data-easing="easeOutExpo"  >24 Hour</div>
                        <div class="caption large_text sfr"
                           data-x="689"
                           data-y="92"
                           data-speed="300"
                           data-start="1100"
                           data-easing="easeOutExpo"  >National</div>
                        <div class="caption large_text sfr"
                           data-x="738"
                           data-y="129"
                           data-speed="300"
                           data-start="1100"
                           data-easing="easeOutExpo"  ><span class="dblue">Poisons Center</span></div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="1400" 
                           data-speed="1050" 
                           data-y="180" 
                           data-x="660">
                           <div class="list-slide">
                              <i class="fa fa-check slide-icon"></i>
                              <h5> Drug information </h5>
                           </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="1800" 
                           data-speed="1200" 
                           data-y="220" 
                           data-x="660">
                           <div class="list-slide">
                              <i class="fa fa-check slide-icon"></i>
                              <h5> Training of pre-registration pharmacists </h5>
                           </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="2200" 
                           data-speed="1350" 
                           data-y="260" 
                           data-x="660">
                           <div class="list-slide">
                              <i class="fa fa-check slide-icon"></i>
                              <h5> Experimental toxicology research and consultancy </h5>
                           </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="2600" 
                           data-speed="1350" 
                           data-y="300" 
                           data-x="660">
                           <div class="list-slide">
                              <i class="fa fa-check slide-icon"></i>
                              <h5> Expert advice on applied toxicology issues </h5>
                           </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="3000" 
                           data-speed="1350" 
                           data-y="340" 
                           data-x="660">
                           <div class="list-slide">
                              <i class="fa fa-check slide-icon"></i>
                              <h5> Training of health personnel in pharmacotherapeutics </h5>
                           </div>
                        </div>
                        <div class="caption lfl"
                           data-x="53"
                           data-y="30"
                           data-speed="500"
                           data-start="1400"
                           data-easing="easeOutExpo">
                           <img src="img/slider/responsive-imac.png" alt="iMac Responsive">
                        </div>
                        <div class="caption lfl"
                           data-x="323"
                           data-y="145"
                           data-speed="600"
                           data-start="1500"
                           data-easing="easeOutExpo">
                           <img src="img/slider/responsive-ipad.png" alt="iPad Responsive">
                        </div>
                        <div class="caption lfl"
                           data-x="372"
                           data-y="253"
                           data-speed="700"
                           data-start="1600"
                           data-easing="easeOutExpo">
                           <img src="img/slider/responsive-iphone.png" alt="iPhone Responsive">
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
            <!-- Slider End--> 
            <!-- Slogan Start-->
            <div class="slogan bottom-pad-small">
               <div class="container">
                  <div class="row">
                     <div class="slogan-content">
                        <div class="col-lg-9 col-md-9">
                           <h2 class="slogan-title">Continuing Education For Pharmacists</h2>
                        </div>
                        <div class="col-lg-3 col-md-3">
                           <div class="get-started">
                              <button class="btn btn-special btn-color pull-right">Get Started Now</button>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slogan End--> 
            <!-- Main Content start-->
            <div class="main-content">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="content-box big ch-item bottom-pad-small">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-arrows-alt"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-arrows-alt"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h3>Publications</h3>
                              <p>
                                 Gadaga LL, Tagwireyi D, Dzangare J, Nhachi CFB (2010). Acute Oral Toxicity and Neurobehavioural Toxicological Effects of a Hydroethanolic Extract of Boophone disticha in Rats. Human and Experimental Toxicology. [IN PRESS MANUSCRIPT]
                              </p>
                              <a href="#">Read More <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="content-box big ch-item bottom-pad-small">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-eye"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-eye"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h3>Upcoming Events</h3>
                              <p>
                                 DaTIS is proud to announce that we will be holding Continuous Professional Development (CPD) workshop for Pharmacists also known as Continuous Education (CE). Attendance is open to all for a fee of $10. Each meeting will award the Pharmacist a total of 5 points.
                              </p>
                              <a href="#">Read More <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="content-box big ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-edit"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-edit"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h3>Blog</h3>
                              <p>
                                 CORPSE EMBALMING FLUIDS USED ON CHICKENS IMPORTED TO ZIMBABWE:
Zimbabwean food outlets are reportedly selling Brazilian chicken imports which have been embalmed with chemicals normally used to preserve corpses, it has been learnt.
                              </p>
                              <a href="#">Read More <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Main Content end-->

            <!-- Product Content Start-->
            <div class="product-lead bottom-pad">
               <div class="container">
                  <div class="row">
                     <div class="col-md-4 col-sm-12 col-xs-12 text-center animate_afl d1">
                        <img src="img/iphone5c.png" alt="iPhone5c">
                     </div>
                     <div class="col-md-8 col-sm-12 col-xs-12 animate_afr d3">
                        <div class="app-service">
                           <h3>BS-200 Chemistry Analyzer</h3>
                           <h4>A floor-standing, discrete and random access clinical chemistry analyzer offering constant 200 tests per hour. </h4>
                           <p>- Discrete, random access, fully automated, bench-top<br>
    - Constant 200 tests per hour, up to 330 tests per hour with ISE<br>
    - Auto washing system with pre-warmed detergent and water<br>
    - Grating optical system<br>
    - 150μl minimum reaction volume<br>
    - Independent mixing bar<br>
    - Reusable reaction cuvettes<br>
    - Bi-directional connection to LIS host


                           </p>
                           <p>
                              Visit our office to get your sampes analysed.
                           </p>
                           <div class="divider"></div>
                           <div class="learnmore">
                              <a class="btn-special btn-grey" href="#">Learn More</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div> 
            <!-- Product Content end--> 
           
         </div>
         <!-- Content End -->
       