<?php /* Smarty version 2.6.10, created on 2018-04-05 04:59:12
         compiled from a_questMan.htm */ ?>
<!--<div id="manage_bottom">
	<div class="box"><a href="index.php?c=news_new"> <img src="images/add_detail.png" /></a> Add Detail</div>
    <div class="box"><a href="#"> <img src="images/edit.png" /></a> Edit</div>
    <div class="box"><a href="#"> <img src="images/nanage.png" /></a> Manage Details</div>
    <div class="box"><a href="#"> <img src="images/delete.png"  /></a> Delete</div>
    </div>-->


<div class="container-fluid">
		<div class="row-fluid">

<div id="content" class="span10">

			
			
			
			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-info-sign"></i> Questions Management</h2>
						
					</div>
					
					<div class="box-content">

<table class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead class="header_table">
				<th  width="2%">Question ID</th>
				<th  width="72%">Question</th>
				<th  width="8%">Type</th>
				<th  width="18%">Action</th>
			
	</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['questions']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>

	<td><?php echo $this->_tpl_vars['questions'][$this->_sections['list']['index']]['id']; ?>
</td>
	<td><?php echo $this->_tpl_vars['questions'][$this->_sections['list']['index']]['question']; ?>
</td>
	<td><?php echo $this->_tpl_vars['questions'][$this->_sections['list']['index']]['section']; ?>
</td>
	<td>
		<a href="do.quest.php?op=D&id=<?php echo $this->_tpl_vars['questions'][$this->_sections['list']['index']]['id']; ?>
" onclick="return confirm('Are you sure you want to delete this record? ');"/><img src="images/icons/delete.png" /></a>
		<a href="index.php?c=edit_mcq&id=<?php echo $this->_tpl_vars['questions'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/edit.png" /></a>
		<a href="do.quest.php?op=S&id=<?php echo $this->_tpl_vars['questions'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/status<?php echo $this->_tpl_vars['questions'][$this->_sections['list']['index']]['active']; ?>
.png" />
		</a>
	</td>
</tr>
<?php endfor; endif; ?>
</tbody>

</table>
<div class="btn-round"><a href="index.php?c=mcq" class="btn btn-special btn-color pull-right"> Add New Question</a></div>
</div>
				</div>
			</div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>