<?php /* Smarty version 2.6.10, created on 2011-01-20 15:41:38
         compiled from box/u_profile.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'box/u_profile.htm', 4, false),array('modifier', 'upper', 'box/u_profile.htm', 45, false),)), $this); ?>
<div class="profileCon">
	<div class="editpro">
		<!--<img src="images/justus.png" class="profile_pic" />-->
		 <img src="<?php echo ((is_array($_tmp=@$this->_tpl_vars['userdata']['person']['picture'])) ? $this->_run_mod_handler('default', true, $_tmp, 'images/pic.png') : smarty_modifier_default($_tmp, 'images/pic.png')); ?>
" class="profile_pic" />
		<div class="editCon">
	        <a href="index.php?c=picture-edit&id=<?php echo $this->_tpl_vars['userdata']['person']['id']; ?>
"  class="profilepic">Edit picture</a>
	        <a href="index.php?c=profile-edit&id=<?php echo $this->_tpl_vars['userdata']['person']['id']; ?>
" class="profileedit">Edit profile</a>
	        <a href="index.php?c=password&id=<?php echo $this->_tpl_vars['userdata']['person']['id']; ?>
" class="profilepassword">Edit password</a>
	        <a href="core/do.logout.php" class="logout">Logout</a>
		</div>
	</div>
	<div class="details">
		<div class="nameCon">
			<div class="name_title">Name:</div>
			<div class="name">  <a href="index.php?c=user_friends" ><?php echo $this->_tpl_vars['userdata']['person']['name']; ?>
 <?php echo $this->_tpl_vars['userdata']['person']['surname']; ?>
</a></div>
		</div>
		<div class="sexCon">
			<div class="sex_title">Sex:</div><div class="sex"> <?php echo $this->_tpl_vars['userdata']['person']['genderText']; ?>
</div></div>
		<div class="dobCon">
			<div class="dob_title">Date of Birth:</div>
			<div class="dob"><?php echo $this->_tpl_vars['userdata']['person']['dateOfBirthDMY']; ?>
 (<?php echo $this->_tpl_vars['userdata']['person']['age']; ?>
)</div>
		</div>
		<div class="poboxCon">
			<div class="pobox_title">P O Box:</div>
			<div class="pobox"> <?php echo $this->_tpl_vars['userdata']['person']['POBox']; ?>
</div>
		</div>
		<div class="emailCon">
			<div class="email_title">Email:</div>
			<div class="email"> <?php echo $this->_tpl_vars['userdata']['person']['email']; ?>
</div>
		</div>
		<div class="mobileCon">
			<div class="mobile_title">Mobile Phone Number:</div>
			<div class="mobile"> <?php echo $this->_tpl_vars['userdata']['person']['mobile']; ?>
</div>
		</div>
		<div class="languagesCon">
			<div class="languages_title">Languages:</div>
			<div class="languages"> <?php echo $this->_tpl_vars['userdata']['person']['languages']; ?>
</div>
		</div>
		<div class="occupationCon">
			<div class="occupation_title">Occupation:</div>
			<div class="occupation"> <?php echo $this->_tpl_vars['userdata']['person']['occupation']; ?>
</div>
		</div>
		<div class="countryCon">
			<div class="country_title">Country:</div>
			<img src="images/countries/<?php echo ((is_array($_tmp=$this->_tpl_vars['userdata']['person']['countryCode'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
_mini.png" class="country_flag" />
			<div class="country"> <?php echo $this->_tpl_vars['userdata']['person']['countryText']; ?>
</div>
			<!--<img src="images/afriflag.jpg" />-->
		</div>
	</div>
</div>