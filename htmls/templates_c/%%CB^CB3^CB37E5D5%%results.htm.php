<?php /* Smarty version 2.6.10, created on 2018-04-05 02:51:07
         compiled from results.htm */ ?>


    
  <fieldset>
    <p>
      <strong>
        Thank you for taking your CPD Test with us. Your test results are displayed below.
      </strong>
    </p>
    <p>
      <ul>
        <li>
          Your Score : <?php echo $this->_tpl_vars['score']; ?>
 % 
        </li>
		 <li>
          Your Grade : <?php if ($this->_tpl_vars['score'] >= $this->_tpl_vars['passmark']): ?> PASS <?php else: ?> FAIL <?php endif; ?> 
        </li>
        <li>
         Total number of Questions : <?php echo $this->_tpl_vars['questions']; ?>

        </li>
        <li>
          Pass Mark : <?php echo $this->_tpl_vars['passmark']; ?>
%
        </li>
		<li>
          Date of Exam : <?php echo $this->_tpl_vars['date']; ?>

        </li>
      </ul>
    </p>
    <p>
     </fieldset>
