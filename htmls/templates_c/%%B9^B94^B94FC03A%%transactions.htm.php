<?php /* Smarty version 2.6.10, created on 2018-04-05 04:52:39
         compiled from transactions.htm */ ?>

<div class="container-fluid">
		<div class="row-fluid">

			<div id="content" class="span10">

						
						
						<div class="row-fluid">
							<div class="box span12">
								<div class="box-header well" data-original-title>
									<h2><i class="icon-info-sign"></i> Transaction Management</h2>
									
								</div>
								<div class="box-content">

			<table class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead class="header_table">
							<th  width="2%">Member</th>
							<th  width="52%">Issue</th>
							<th  >Amount</th>
							<th  width="18%"> Date</th>	
							<th >Reference</th>
							<th >Status</th>
						<!--<?php if ($this->_tpl_vars['userdata']['profile'] == A): ?>
						<th  width="18%">Action</th>
						<?php endif; ?>-->
						
				</thead>
			<tbody>
			<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['transaction']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>

			<tr>

				<td><?php echo $this->_tpl_vars['transaction'][$this->_sections['list']['index']]['name']; ?>
 <?php echo $this->_tpl_vars['transaction'][$this->_sections['list']['index']]['surname']; ?>
</td>
				<td><?php echo $this->_tpl_vars['transaction'][$this->_sections['list']['index']]['Title']; ?>
 </td>
				<td>$<?php echo $this->_tpl_vars['transaction'][$this->_sections['list']['index']]['amount']; ?>
</td>
				<td><?php echo $this->_tpl_vars['transaction'][$this->_sections['list']['index']]['paydate']; ?>
</td>
				<td><?php echo $this->_tpl_vars['transaction'][$this->_sections['list']['index']]['reference']; ?>
</td>
				<td><?php echo $this->_tpl_vars['transaction'][$this->_sections['list']['index']]['paymentStatus']; ?>
</td>
			<!--	<?php $this->assign('count', 0); ?>
				<?php $this->assign('ref', 0); ?>
				
				<?php unset($this->_sections['listb']);
$this->_sections['listb']['name'] = 'listb';
$this->_sections['listb']['loop'] = is_array($_loop=$this->_tpl_vars['issues']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['listb']['show'] = true;
$this->_sections['listb']['max'] = $this->_sections['listb']['loop'];
$this->_sections['listb']['step'] = 1;
$this->_sections['listb']['start'] = $this->_sections['listb']['step'] > 0 ? 0 : $this->_sections['listb']['loop']-1;
if ($this->_sections['listb']['show']) {
    $this->_sections['listb']['total'] = $this->_sections['listb']['loop'];
    if ($this->_sections['listb']['total'] == 0)
        $this->_sections['listb']['show'] = false;
} else
    $this->_sections['listb']['total'] = 0;
if ($this->_sections['listb']['show']):

            for ($this->_sections['listb']['index'] = $this->_sections['listb']['start'], $this->_sections['listb']['iteration'] = 1;
                 $this->_sections['listb']['iteration'] <= $this->_sections['listb']['total'];
                 $this->_sections['listb']['index'] += $this->_sections['listb']['step'], $this->_sections['listb']['iteration']++):
$this->_sections['listb']['rownum'] = $this->_sections['listb']['iteration'];
$this->_sections['listb']['index_prev'] = $this->_sections['listb']['index'] - $this->_sections['listb']['step'];
$this->_sections['listb']['index_next'] = $this->_sections['listb']['index'] + $this->_sections['listb']['step'];
$this->_sections['listb']['first']      = ($this->_sections['listb']['iteration'] == 1);
$this->_sections['listb']['last']       = ($this->_sections['listb']['iteration'] == $this->_sections['listb']['total']);
?>
				
				<?php if ($this->_tpl_vars['volume'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['issues'][$this->_sections['listb']['index']]['volume']): ?>
				<?php $this->assign('count', $this->_tpl_vars['count']+1); ?>
				<?php $this->assign('ref', $this->_tpl_vars['issues'][$this->_sections['listb']['index']]['count']); ?>
				<?php endif; ?>
				
				
				<?php endfor; endif; ?>
				
				<?php if ($this->_tpl_vars['count'] > 0): ?>
				<a href="index.php?c=issues&vol=<?php echo $this->_tpl_vars['volume'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['ref']; ?>
</a>
				<?php elseif ($this->_tpl_vars['userdata']['profile'] == A): ?>
				<a href="index.php?c=new_issues&op=I">Add New issue</a>
				<?php elseif ($this->_tpl_vars['userdata']['profile'] == S): ?>
				Coming soon
				<?php endif; ?>
				
				
				</td>
				<?php if ($this->_tpl_vars['userdata']['profile'] == A): ?>
				<td>
					<a href="do.quest.php?op=D&id=<?php echo $this->_tpl_vars['volume'][$this->_sections['list']['index']]['id']; ?>
" onclick="return confirm('Are you sure you want to delete this record? ');"/><img src="images/icons/delete.png" /></a>
					<a href="index.php?c=vol&id=<?php echo $this->_tpl_vars['volume'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/edit.png" /></a>
					<a href="do.quest.php?op=S&id=<?php echo $this->_tpl_vars['volume'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/status<?php echo $this->_tpl_vars['volume'][$this->_sections['list']['index']]['active']; ?>
.png" />
					</a>
				</td>
				<?php endif; ?>-->
			</tr>
			<?php endfor; endif; ?>
			</tbody>

			</table>
			</div>
							</div>
						</div>
			</div>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<!--<?php if ($this->_tpl_vars['userdata']['profile'] == A): ?>
				<div class="btn-round"><a href="index.php?c=vol" class="btn btn-special btn-color pull-right"> Add Volume</a> </div>
				<?php endif; ?>-->
		</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>