<?php /* Smarty version 2.6.10, created on 2018-04-05 05:00:30
         compiled from a_issue.htm */ ?>
<div class="container-fluid">
		<div class="row-fluid">

<div id="content" class="span10">
				
			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-info-sign"></i> Issue Management</h2>

					</div>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/alert.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					<div class="box-content">

<table class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead class="header_table">
				<th  width="2%">Issue ID</th>
				<th  width="52%">Title</th>
				<th  width="18%">Volume</th>
				<th  width="18%">Publish Date</th>	
				<!--<th >No. of images</th>-->
				
				<th  width="18%">Action</th>
			
	</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['issue']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>

	<td><?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid']; ?>
</td>
	<td><a href="index.php?c=issue_view&id=<?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid']; ?>
"><?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['Title']; ?>
 </a></td>
	<td><?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['name']; ?>
</td>
	<td><?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['insTS']; ?>
</td>
	<!--<td>
	<?php unset($this->_sections['listb']);
$this->_sections['listb']['name'] = 'listb';
$this->_sections['listb']['loop'] = is_array($_loop=$this->_tpl_vars['images']/$this->_tpl_vars['mg']/$this->_tpl_vars['g']-$this->_tpl_vars['l']-$this->_tpl_vars['op']['gifs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['listb']['show'] = true;
$this->_sections['listb']['max'] = $this->_sections['listb']['loop'];
$this->_sections['listb']['step'] = 1;
$this->_sections['listb']['start'] = $this->_sections['listb']['step'] > 0 ? 0 : $this->_sections['listb']['loop']-1;
if ($this->_sections['listb']['show']) {
    $this->_sections['listb']['total'] = $this->_sections['listb']['loop'];
    if ($this->_sections['listb']['total'] == 0)
        $this->_sections['listb']['show'] = false;
} else
    $this->_sections['listb']['total'] = 0;
if ($this->_sections['listb']['show']):

            for ($this->_sections['listb']['index'] = $this->_sections['listb']['start'], $this->_sections['listb']['iteration'] = 1;
                 $this->_sections['listb']['iteration'] <= $this->_sections['listb']['total'];
                 $this->_sections['listb']['index'] += $this->_sections['listb']['step'], $this->_sections['listb']['iteration']++):
$this->_sections['listb']['rownum'] = $this->_sections['listb']['iteration'];
$this->_sections['listb']['index_prev'] = $this->_sections['listb']['index'] - $this->_sections['listb']['step'];
$this->_sections['listb']['index_next'] = $this->_sections['listb']['index'] + $this->_sections['listb']['step'];
$this->_sections['listb']['first']      = ($this->_sections['listb']['iteration'] == 1);
$this->_sections['listb']['last']       = ($this->_sections['listb']['iteration'] == $this->_sections['listb']['total']);
?>
	<?php if ($this->_tpl_vars['issue'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['images']/$this->_tpl_vars['mg']/$this->_tpl_vars['g']-$this->_tpl_vars['l']-$this->_tpl_vars['op']['gifs'][$this->_sections['listb']['index']]['issue']): ?>
	<a href="index.php?c=images/img/bg-pl-top.gifs&vol=<?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['images']/$this->_tpl_vars['mg']/$this->_tpl_vars['g']-$this->_tpl_vars['l']-$this->_tpl_vars['op']['gifs'][$this->_sections['listb']['index']]['count']; ?>
</a>
	<?php endif; ?>
	<?php endfor; endif; ?>
	</td>-->
	
	<?php if ($this->_tpl_vars['userdata']['profile'] == S): ?>
	
	<?php if (in_array ( $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid'] , $this->_tpl_vars['answered'] )): ?>
	
<?php unset($this->_sections['record']);
$this->_sections['record']['name'] = 'record';
$this->_sections['record']['loop'] = is_array($_loop=$this->_tpl_vars['ans']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['record']['show'] = true;
$this->_sections['record']['max'] = $this->_sections['record']['loop'];
$this->_sections['record']['step'] = 1;
$this->_sections['record']['start'] = $this->_sections['record']['step'] > 0 ? 0 : $this->_sections['record']['loop']-1;
if ($this->_sections['record']['show']) {
    $this->_sections['record']['total'] = $this->_sections['record']['loop'];
    if ($this->_sections['record']['total'] == 0)
        $this->_sections['record']['show'] = false;
} else
    $this->_sections['record']['total'] = 0;
if ($this->_sections['record']['show']):

            for ($this->_sections['record']['index'] = $this->_sections['record']['start'], $this->_sections['record']['iteration'] = 1;
                 $this->_sections['record']['iteration'] <= $this->_sections['record']['total'];
                 $this->_sections['record']['index'] += $this->_sections['record']['step'], $this->_sections['record']['iteration']++):
$this->_sections['record']['rownum'] = $this->_sections['record']['iteration'];
$this->_sections['record']['index_prev'] = $this->_sections['record']['index'] - $this->_sections['record']['step'];
$this->_sections['record']['index_next'] = $this->_sections['record']['index'] + $this->_sections['record']['step'];
$this->_sections['record']['first']      = ($this->_sections['record']['iteration'] == 1);
$this->_sections['record']['last']       = ($this->_sections['record']['iteration'] == $this->_sections['record']['total']);
?>

<?php if ($this->_tpl_vars['ans'][$this->_sections['record']['index']]['issue_no'] == $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid']): ?>
<td> <a href="index.php?c=results&id=<?php echo $this->_tpl_vars['ans'][$this->_sections['record']['index']]['id']; ?>
" >Results</a></td>
<?php endif; ?>
    <!--<?php $_from = $this->_tpl_vars['ans'][$this->_sections['record']['index']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['entry']):
?>
	
       <?php echo $this->_tpl_vars['entry']; ?>

    <?php endforeach; endif; unset($_from); ?>-->
 
<?php endfor; endif; ?>
	

<?php else: ?>
	<td> <a href="index.php?c=front&isu=<?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid']; ?>
" >Take Exam</a></td>
	<?php endif; ?>
	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['userdata']['profile'] == A): ?>
	<td>
		<a href="do.quest.php?op=D&id=<?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid']; ?>
" onclick="return confirm('Are you sure you want to delete this record? ');"/><img src="images/icons/delete.png" /></a>
		<a href="index.php?c=new_issues&id=<?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid']; ?>
" /><img src="images/icons/edit.png" /></a>
		<a href="do.quest.php?op=S&id=<?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['iid']; ?>
" /><img src="images/icons/status<?php echo $this->_tpl_vars['issue'][$this->_sections['list']['index']]['active']; ?>
.png" />
		</a>
	</td>
	<?php endif; ?>
</tr>
<?php endfor; endif; ?>
</tbody>

</table>

</div>
				</div>
			</div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php if ($this->_tpl_vars['userdata']['profile'] == A): ?>
<div class="btn-round"><a href="index.php?c=new_issues&op=I" class="btn btn-special btn-color pull-right"> Add Issue</a> </div>
<?php endif; ?>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>