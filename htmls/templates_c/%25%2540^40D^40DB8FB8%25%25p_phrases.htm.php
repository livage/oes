<?php /* Smarty version 2.6.10, created on 2011-01-14 07:21:47
         compiled from p_phrases.htm */ ?>
<p class="pageTitle">Daily Phrases (List)</p>

<table class="list">
<thead>
<tr>
	<td>Date</td>
	<td>Title</td>
	<td>Text</td>
	<td>Actions</td>
</tr>
</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['phrases']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>
	<td><?php echo $this->_tpl_vars['phrases'][$this->_sections['list']['index']]['dateDMY']; ?>
</td>
	<td><?php echo $this->_tpl_vars['phrases'][$this->_sections['list']['index']]['title']; ?>
</td>
	<td><?php echo $this->_tpl_vars['phrases'][$this->_sections['list']['index']]['text']; ?>
</td>
	<td>
		<a href="index.php?c=phrase-edit&id=<?php echo $this->_tpl_vars['phrases'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/edit.png" /></a>
		<a href="javascript:phraseDelete(<?php echo $this->_tpl_vars['phrases'][$this->_sections['list']['index']]['id']; ?>
)" /><img src="images/icons/delete.png" /></a>
		<a href="do.phrases.php?op=S&id=<?php echo $this->_tpl_vars['phrases'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/status<?php echo $this->_tpl_vars['phrases'][$this->_sections['list']['index']]['active']; ?>
.png" /></a>
	</td>
</tr>
<?php endfor; endif; ?>
</tbody>
<!--<tfoot>
<tr>
	<td>Username</td>
	<td>Name</td>
	<td>Profile</td>
</tr>
</tfoot>-->
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<a href="index.php?c=phrase-new" id="buttonNew">New Phrase</a>