<?php /* Smarty version 2.6.10, created on 2018-04-05 04:58:42
         compiled from issue_edit.htm */ ?>
<div class="container-fluid">
		<div class="row-fluid">

<div id="content" class="span10">

			
			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-info-sign"></i> Issue Management</h2>
						
					</div>
					<div class="box-content">

<script type="text/javascript" src="js/hide.js" ></script>
<div id="formpage_1" name="insert" style="visibility: visible; display: block; .."> 
  
 <form action="do_issue.php?op=<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
" method="post" enctype="multipart/form-data">

	  <div id="content">
        <fieldset class="manufacturer">
		<legend>Issue</legend>
		
		<!--
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Name: <span>*</span></label>
                                    <input type="text" required="" value="" name="name" id="name" class="form-control">
                                 </div>
		-->
		<p>
			
			 <label>	Volume :<span>*</span></label>
			<select name="volume" required  class="form-control">
			<option value="" >Select an Volume</option>
			<?php unset($this->_sections['listb']);
$this->_sections['listb']['name'] = 'listb';
$this->_sections['listb']['loop'] = is_array($_loop=$this->_tpl_vars['volume']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['listb']['show'] = true;
$this->_sections['listb']['max'] = $this->_sections['listb']['loop'];
$this->_sections['listb']['step'] = 1;
$this->_sections['listb']['start'] = $this->_sections['listb']['step'] > 0 ? 0 : $this->_sections['listb']['loop']-1;
if ($this->_sections['listb']['show']) {
    $this->_sections['listb']['total'] = $this->_sections['listb']['loop'];
    if ($this->_sections['listb']['total'] == 0)
        $this->_sections['listb']['show'] = false;
} else
    $this->_sections['listb']['total'] = 0;
if ($this->_sections['listb']['show']):

            for ($this->_sections['listb']['index'] = $this->_sections['listb']['start'], $this->_sections['listb']['iteration'] = 1;
                 $this->_sections['listb']['iteration'] <= $this->_sections['listb']['total'];
                 $this->_sections['listb']['index'] += $this->_sections['listb']['step'], $this->_sections['listb']['iteration']++):
$this->_sections['listb']['rownum'] = $this->_sections['listb']['iteration'];
$this->_sections['listb']['index_prev'] = $this->_sections['listb']['index'] - $this->_sections['listb']['step'];
$this->_sections['listb']['index_next'] = $this->_sections['listb']['index'] + $this->_sections['listb']['step'];
$this->_sections['listb']['first']      = ($this->_sections['listb']['iteration'] == 1);
$this->_sections['listb']['last']       = ($this->_sections['listb']['iteration'] == $this->_sections['listb']['total']);
?>
	<option value="<?php echo $this->_tpl_vars['volume'][$this->_sections['listb']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['volume'][$this->_sections['listb']['index']]['id'] == $this->_tpl_vars['issue']['0']['volume']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['volume'][$this->_sections['listb']['index']]['name']; ?>
</option>
	<?php endfor; endif; ?>
			</select>
			
			 
			 </p>
			 <p>
				
 <label>Issue  Title :<span>*</span></label>
			<input type="text" name="title" size="70" class="form-control" value ="<?php echo $this->_tpl_vars['issue']['0']['Title']; ?>
" required/>
			 
			 </p>
			 
	        <p>
					
			 	 <label>PDF Upload:<span>*</span></label>
			 <input type="file" name="points" size="70"  required> 
			 
		</p>
			 
        </fieldset> 
		<p></p>
		<p>
	    <input type="submit" value="Confirm"  class="btn-small btn-color btn-pad"/>
		
	    <input type="button" value="Cancel"  class="btn-small btn-color btn-pad" href="question_list.php" />
		</p>
	 </div>
</form>

</div>
</div>
				</div>
			</div>
</div>

</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
