<?php /* Smarty version 2.6.10, created on 2018-04-05 05:00:30
         compiled from box/alert.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strpos', 'box/alert.htm', 2, false),)), $this); ?>
<?php if ($this->_tpl_vars['msg'] != ""): ?>
			<?php if (((is_array($_tmp=$this->_tpl_vars['msg'])) ? $this->_run_mod_handler('strpos', true, $_tmp, 'Error') : strpos($_tmp, 'Error')) === 0): ?>
			
				<div class="alert alert-danger alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Error!</strong> <?php echo $this->_tpl_vars['msg']; ?>
.
				</div>
				
			<?php else: ?> 
				 <div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong>Your <?php echo $this->_tpl_vars['msg']; ?>
 was successful!!.
				</div>
			<?php endif; ?>
		<?php endif; ?>