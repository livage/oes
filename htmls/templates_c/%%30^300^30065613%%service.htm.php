<?php /* Smarty version 2.6.10, created on 2017-04-12 10:48:57
         compiled from service.htm */ ?>
<!-- Content Start -->
         <div id="main">
            <!-- Title, Breadcrumb Start-->
            <div class="breadcrumb-wrapper">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        
                    </div>
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <div class="breadcrumbs pull-right">
                           <ul>
                              <li>You are here:</li>
                              
                             <li>Services</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Title, Breadcrumb End-->
            <!-- Main Content start-->
            <div class="divider"></div>
            <!-- 3 Column Big Services -->
            <div class="services-big">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="title">Our Services</h3>
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="content-box">
                           <div class="content-box-icon">
                              <img src="img/portfolio/portfolio-6.jpg" alt=" "/>
                           </div>
                           <div class="content-box-info">
                              <h4>Consultacy</h4>
                              <p>
                                 Consultation in therapeutic drug monitoring and clinical analytical toxicology: Therapeutic drug monitoring (TDM) is a branch of clinical chemistry that specializes in the measurement of medication levels in blood. Its main focus is on drugs with a narrow therapeutic range, i.e. drugs that can easily be under- oroverdosed.
                             </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="content-box">
                           <div class="content-box-icon">
                              <img src="img/portfolio/portfolio-3.jpg" alt=" "/>
                           </div>
                           <div class="content-box-info">
                              <h4>Teaching</h4>
                              <p>
                                 Teaching of pharmacy and other students on drug and poisons related issues
                             </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="content-box">
                           <div class="content-box-icon">
                              <img src="img/portfolio/portfolio-7.jpg" alt=" "/>
                           </div>
                           <div class="content-box-info">
                              <h4>Research</h4>
                              <p>
                                 Experimental toxicology research and consultancy
                             </p>
                           </div>
                        </div>
                     </div>
                     <!-- 3 Column Services End-->
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
            
            <!-- Slogan Start--><!-- Slogan End--> 
           <div class="divider"></div>
            <!-- 3 Column Services -->
            <div class="services">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="title">More Services</h3>
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-lg-4 col-md-4 col-sm-4 bottom-pad">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-eye"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-eye"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h4>Poison Centre</h4>
                              <p>
                                 
    A free 24-hour national poisons information service

                             </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 bottom-pad">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-edit"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-edit"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h4>Drug information</h4>
                              <p>
                                 We provide Drug Information at no cost
                             </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-leaf"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-leaf"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h4>Training</h4>
                              <p>
                                 Training of pre-registration pharmacists
                             </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-cog"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-cog"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h4>National Training</h4>
                              <p>
                                 National and regional training of health personnel in pharmacotherapeutics and toxicology related issues
                             </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"><i class="fa fa-search"></i></div>
                                 <div class="ch-info-back">
                                    <i class="fa fa-search"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h4>Toxicology Advice</h4>
                              <p>
                                 Expert advice on applied toxicology issues to the private sector
                             </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="divider"></div>
            <!-- Main Content end-->
         </div>
         <!-- Content End -->