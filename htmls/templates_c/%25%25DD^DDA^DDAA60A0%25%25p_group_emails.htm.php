<?php /* Smarty version 2.6.10, created on 2011-01-14 07:22:16
         compiled from p_group_emails.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'p_group_emails.htm', 16, false),)), $this); ?>
<p class="pageTitle">Group E-Mails (List)</p>

<table class="list">
<thead>
<tr>
	<td>Subject</td>
	<td>Message</td>
	<td>Status</td>
	<td>Actions</td>
</tr>
</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['emails']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>
	<td><?php echo $this->_tpl_vars['emails'][$this->_sections['list']['index']]['subject']; ?>
</td>
	<td><?php echo ((is_array($_tmp=$this->_tpl_vars['emails'][$this->_sections['list']['index']]['message'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)); ?>
</td>
	<td><?php echo $this->_tpl_vars['emails'][$this->_sections['list']['index']]['status']; ?>
</td>
	<td>
	<?php if ($this->_tpl_vars['emails'][$this->_sections['list']['index']]['mode'] == 'W'): ?>
		<a href="index.php?c=group-email-edit&id=<?php echo $this->_tpl_vars['emails'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/edit.png" /></a>
		<a href="javascript:emailDelete(<?php echo $this->_tpl_vars['emails'][$this->_sections['list']['index']]['id']; ?>
)" /><img src="images/icons/delete.png" /></a>
	<?php endif; ?>
	</td>
</tr>
<?php endfor; endif; ?>
</tbody>
<!--<tfoot>
<tr>
	<td>Username</td>
	<td>Name</td>
	<td>Profile</td>
</tr>
</tfoot>-->
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<a href="index.php?c=group-email-new" id="buttonNew">New Group Email</a>