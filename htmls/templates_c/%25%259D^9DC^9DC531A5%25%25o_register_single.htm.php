<?php /* Smarty version 2.6.10, created on 2011-01-20 15:41:13
         compiled from o_register_single.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'o_register_single.htm', 35, false),)), $this); ?>
<form action="do.register.single.php" method="post">
	<div class="title">Sign Up</div>
	<div class="break"></div>

	<div class="boxfield">
	    <label>Name</label>
	    <input type="text" name="name" maxlength="255" />
	</div>
	<div class="boxfield">
	    <label>Surname</label>
	    <input type="text" name="surname" maxlength="255" />
	</div>

	<div class="break"></div>

	<!--<div class="boxfield">
		<label>Picture:</label>
	    <div class="imgCon"></div>
		<input type="file" name="picture" />
	</div>-->

                <div class="boxfield">
					<label>Picture</label>
	                <input type="hidden" class="name"  name="picture" maxlength="255"  readonly >
					<input type="file" class="name"  name="picture" maxlength="255" id="photo" readonly >
				</div>

					
	<div class="break"></div>

	<div class="boxfield">
	    <label>Gender:</label>
	    <select name="gender">
	    	<option value=""></option>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['_lists']['gender'],'separator' => ' '), $this);?>

	    </select>
	</div>
	<div class="boxfield date">
	    <label>Date of Birth</label>
	    <input class="_fDate" type="text" name="dateOfBirth" />
	    <!--<img src="images/calendar.png" class="calendar" />-->
	</div>
	<div class="boxfield date">
		<label>Marital status</label>
		<select name="maritalStatus">
			<option value=""></option>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['_lists']['marital']), $this);?>

		</select>
	</div>
	<div class="boxfield">
		<label> Wedding Date</label>
		<input class="_fDate" type="text" name="weddingDate" />
		<!--<img src="images/calendar.png" class="calendar" />-->
	</div>

	<div class="break"></div>

	<div class="boxfield">
		<label>Email:</label>
		<input type="text" name="email" class="_fEmail" maxlength="255" />
	</div>

	<div class="break"></div>

	<div class="boxfield">
		<label>Country:</label>
		<select name="country" class="_fCountry">
	        <option value=""></option>
	        <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['_lists']['countries']), $this);?>

	    </select>
	</div>

	<div class="break"></div>

	<div class="boxfield">
		<label>City</label>
		<input type="text" name="city" maxlength="255" />
	</div>

	<div class="boxfield">
	    <label>P O Box</label>
	    <input type="text" name="pobox" maxlength="30" />
	</div>

	<div class="break"></div>

	<div class="boxfield">
		<label>Citizenship:</label>
		<input type="text" name="citizenship" maxlength="255" />
	</div>

	<div class="break"></div>

	<div class="boxfield">
		<label>Languages:</label>
		<select name="languages">
	    	<option value=""></option>
			<option value="1"></option>
			<option value="2"></option>
			<option value="3"></option>
			<option value="4"></option>
		</select>
	</div>

	<div class="break"></div>

	<div class="boxfield">
	    <label>Mobile Phone Number:</label>
	    <input type="text" name="mobile" class="_fPhoneNumber" maxlength="60" />
	</div>

	<div class="boxfield">
	    <label>Occupation</label>
	    <input type="text" name="occupation" maxlength="255" />
	</div>

	<div class="break"></div>

	<input type="submit" name="button" id="button" value="Register" />
</form>