<link rel="stylesheet" href="css/calendar1.css" type="text/css" />
<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');



/* draws a calendar */
//function draw_calendar($month,$year){

$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 

"August", "September", "October", "November", "December");

if ($_REQUEST["month"]==0) $_REQUEST["month"] = date("n");

if ($_REQUEST["year"]==0) $_REQUEST["year"] = date("Y");


$month = $_REQUEST["month"];

$year = $_REQUEST["year"];



$prev_year = $year;

$next_year = $year;

$prev_month = $month-1;

$next_month = $month+1;



if ($prev_month == 0 ) {

	$prev_month = 12;

	$prev_year = $year - 1;

}

if ($next_month == 13 ) {

	$next_month = 1;

	$next_year = $year + 1;

}






	/* draw table */
	
	
$calendar .='<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>

<td width="10%" align="left">  <div onclick=" return calendar('.$prev_month.', '.$prev_year.') " style="color:red">Previous</div></td>
<td colspan="7" width="80%" align="center" bgcolor="#999999" style="color:black"><strong>'. $monthNames[$month-1].' '.$year.'</strong></td>
<td width="10%" align="right"><div  onclick=" return calendar('.$next_month.','.$next_year.') " style="color:red">Next</div>  </td>

</tr>

</table>';
	
	$calendar .= '<table cellpadding="0" align="center" cellspacing="0" class="calendar">';

	/* table headings */
	$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();

	/* row for week one */
	$calendar.= '<tr class="calendar-row">';

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td class="calendar-day-np"> </td>';
		$days_in_this_week++;
	endfor;
	
	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		$calendar.= '<td class="calendar-day">';
			/* add in the day number */
		$weekDay = date('N', mktime(0,0,0,$month,$list_day,$year));
		
			if(date(mktime(0,0,0,$month,$list_day,$year)) <= mktime(0,0,0,date("m"),date("d"),date("Y")))
			$calendar.= '<div class="today-number">'.$list_day.'</div>';
			else
			$calendar.= '<div class="day-number">'.$list_day.'</div>';

			/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
			$list_date=date(mktime(0,0,0,$month,$list_day,$year));
			//echo ;
			$sql = 'SELECT COUNT(*) AS day_bookings FROM bookings WHERE test_date ="'.date("Y-m-d",$list_date).'"';
			//echo $sql;
			$bookings = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			
			if ($bookings[0]['day_bookings'] >= SESSION_SIZE)
			$calendar.= "<p> Full </p>";
			elseif((date(mktime(0,0,0,$month,$list_day,$year)) > mktime(0,0,0,date("m"),date("d"),date("Y"))) && $weekDay != 7 && $weekDay != 6)
			$calendar.= '<p> <a href="index.php?c=booking&date='.date(mktime(0,0,0,$month,$list_day,$year)).'" >Available'.$weekDay.'</a> </p>';
			else
			$calendar.= '<p> </p>';
		$calendar.= '</td>';
		if($running_day == 6):
			$calendar.= '</tr>';
			if(($day_counter+1) != $days_in_month):
				$calendar.= '<tr class="calendar-row">';
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			$calendar.= '<td class="calendar-day-np"> </td>';
		endfor;
	endif;

	/* final row */
	$calendar.= '</tr>';

	/* end the table */
	$calendar.= '</table>';
	
	/* all done, return result */
	//return $calendar;
//}

$resultSet=$calendar;

//$resultSet=draw_calendar(,2013);
echo $resultSet;

?>