<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');


if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}

echo $op; exit;
$fields = array(
	'STRING' => array(
		'name',
		'intials',
		'surname',
		'profession',
		'pczno',
		'phone',
		'email',
		'city',
		
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
		
	),
	'DATETIME' => array(
	),
);

$field = array(
	'STRING' => array(
		'username',
		'user_password',
		'profile',
		'email',
		'active',
		'userid',
		'name',
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
		
	),
	'DATETIME' => array(
	),
);

if ($_POST['user_password'] == '') {
	unset($_POST['user_password']);
}

$inputs=array("username"=>$_POST['pczno']."by","user_password"=>$_POST['newpassword'],"profile"=>'S',"email"=>$_POST['email'],"active"=>'0',"userid"=>$id,"name"=> $_POST['name']);
		
$mainTable = 'users';
if ($op == "I")
	$nextPage = 'index.php?c=login';
else
	$nextPage = 'index.php?c=users';

switch ($op) {
      case 'I': // Inserimento
      
			if ($id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError)) {
	
		doOperation($connection, 'I', '_users', $field, $inputs, $sqlError);
		
				if ($_FILES['image']['name']) {
					//require('do.news.saveImage.php');
									define('filesDir','upload/profile/');				
									$allowedExts = array("gif", "jpeg", "jpg", "png");
									$temp = explode(".", $_FILES["image"]["name"]);
									$extension = end($temp);
									//echo $_FILES["image"]["size"];
									if ((($_FILES["image"]["type"] == "image/gif")
									|| ($_FILES["image"]["type"] == "image/jpeg")
									|| ($_FILES["image"]["type"] == "image/jpg")
									|| ($_FILES["image"]["type"] == "image/pjpeg")
									|| ($_FILES["image"]["type"] == "image/x-png")
									|| ($_FILES["image"]["type"] == "image/png"))
									//&& ($_FILES["image"]["size"] < 20000)
									&& in_array($extension, $allowedExts)) 
									{										
										  if ($_FILES["image"]["error"] > 0) {
											echo "Error: " . $_FILES["image"]["error"] . "<br>";
										  } else {
												/*echo "Upload: " . $_FILES["image"]["name"] . "<br>";
												echo "Type: " . $_FILES["image"]["type"] . "<br>";
												echo "Size: " . ($_FILES["image"]["size"] / 1024) . " kB<br>";
												echo "Stored in: " . $_FILES["image"]["tmp_name"];*/
												$filesFileTempname=$_FILES["image"]['tmp_name'];
												//echo $filesFileTempname;
												 move_uploaded_file($filesFileTempname,filesDir."pp".$id.'.'.$extension);
												 $sql = "UPDATE	users	SET 	image='$extension'	WHERE 	id = $id";
												 $db->query($sql);
											  }
										}
					
				}
				
			}
			
			
						// the message
			$msg = "Welcome to ";

			// use wordwrap() if lines are longer than 70 characters
			$msg = wordwrap($msg,70);

			// send email
			mail($_POST['email'],"Registration Verification",$msg);
			
		break;
		case "U": // Aggiornamento
	    	
			if ($id = doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id)) {
				doOperation($connection, 'U', '_users', $field, $inputs, $sqlError, $id);
			}
    	break;
	case "D": 
						
			doOperation($connection, 'D', '_users', $field, $inputs, $sqlError, $id);
			
		      break;
	case 'S':
			doOperation($connection, 'S', "_users", $field, $inputs, $sqlError, $id);
			
		break;
}
?>
<script language="javascript" type="text/javascript">
window.location = "<?php echo $nextPage?>";
</script>