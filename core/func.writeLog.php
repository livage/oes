<?php
function writeLog(  $prmConnection,
                    $prmPage,
                    $prmParameters,
                    $prmStatement,
                    $prmMessage,
                    $prmTreart=array(),
                    $prmFileDetail,
                    $prmTypeCommand,
                    $prmError = 0) {

	$sql =	"	INSERT INTO _log (
					timestamp,
					page_master,
					page_detail,
					parameters,
					templateHTML,
					templatePHP,
					contentHTML,
					contentPHP,
					username,
					type_command,
					statement,
					error,
					message
				) VALUES (
					NOW(),
					'".mysql_real_escape_string($prmPage)."',
					'".mysql_real_escape_string($prmFileDetail)."',
					'".mysql_real_escape_string($prmParameters)."',
					'".mysql_real_escape_string($prmTreart["templateHTML"])."',
					'".mysql_real_escape_string($prmTreart["templatePHP"])."',
					'".mysql_real_escape_string($prmTreart["contentHTML"])."',
					'".mysql_real_escape_string($prmTreart["contentPHP"])."',
					'".mysql_real_escape_string($_SESSION[SITE_NAME]["login_data"]["username"])."',
					'".mysql_real_escape_string($prmTypeCommand)."',
					'".mysql_real_escape_string($prmStatement)."',
					".intval($prmError).",
					'".mysql_real_escape_string($prmMessage)."'
				) ";
    if (!mysql_query($sql, $prmConnection)) {
    	echo $sql . "<br />";
    	echo mysql_error();
    	exit;
    }

}
?>