<?php
function mysqlDateTime($prmDateTime) {
if ($prmDateTime) {
    list($data, $ora) = explode(" ", $prmDateTime);

    // Elabora il giorno
    list ($giorno, $mese, $anno) = explode("/", $data);
    if (	is_numeric($mese)
    	&&	is_numeric($giorno)
    	&&	is_numeric($anno)
    	&&	checkdate($mese, $giorno, $anno)) {
        $retValueDate = "'$anno-$mese-$giorno";

        // Elabora l'ora
        list($ora, $minuti, $secondi) = explode(":", $ora);
        if (	is_numeric($ora) && $ora >= 0 && $ora < 24
        	&& 	is_numeric($minuti) && $minuti >= 0 && $minuti < 60
        	&& 	is_numeric($secondi) && $secondi >= 0 && $secondi < 60) {
            $retValue = $retValueDate." ".
                        str_pad($ora, 2, "0", STR_PAD_LEFT).":".
                        str_pad($minuti, 2, "0", STR_PAD_LEFT).":".
                        str_pad($secondi, 2, "0", STR_PAD_LEFT).
                        "'";
        } else
            $retValue = "NULL";
    } else
        $retValue = "NULL";
} else
    $retValue = "NULL";

return $retValue;

}
?>