<?php
function getListOptions($prmList, $prmlen='it',$prmOrderBy='V') {

	global $connection;
	global $sqlCount;
	global $sqlError;

    switch ($prmOrderBy) {
        case "I":
                $sqlOrder = " ORDER BY list_value ".(substr($prmOrderBy,1)=='D'?'DESC':'');
            break;
        case "V":
                $sqlOrder = " ORDER BY list_label ".(substr($prmOrderBy,1)=='D'?'DESC':'');
            break;
    }
    $options = array();
    $sql =  "   SELECT  e.list_value, e.list_label
    			FROM    _lists AS l
    				JOIN
    					_list_values AS e
					ON  l.id = e.list_id
				WHERE   e.active = 1 AND language = '".$prmlen."' AND list_name = '$prmList' $sqlOrder";

    if ($data = sqlExecute($connection,$sql,$sqlError,$sqlCount,basename(__FILE__),DEBUG))
        for ($i=0; $i<count($data); $i++)
            $options[$data[$i]["list_value"]] = $data[$i]["list_label"];

    return $options;
}
?>