<?php
require('../inc.init.php');
require('core/inc.config.php');

require('class.tmpl_engine.php');
$tmplEngine = new T_tmpl_engine();

if ($_GET['c']) {
	$sql = 'SELECT	title, keywords, content_page, php_page, operation, content_class
			FROM  	_contents
			WHERE   template_id = 0
			AND		content_name = \''.mysql_real_escape_string($_GET['c']).'\'
			AND     access_type LIKE \'%'.$_SESSION[SITE_NAME]['login_data']['profile'].'%\'
			AND     active = 1';
	$contentData = sqlExecute(	$connection,
                                array(  'rowLimit'      => 1,
                                        'sqlStatement'  => $sql ),
                                $sqlError,
                                $sqlCount,
                                basename(__FILE__),
                                DEBUG);
	require_once("func.writeLog.php");
    if (!$contentData)
        writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "No content specified", "Missing Content" . $_TREART["content"], $_TREART, basename(__FILE__),'E',1);
    else {
        writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking content", "Content " .DEFAULT_PAGE . " found", $_TREART, basename(__FILE__),'E',0);

        $_TREART = array(
			// Content
			'contentHTML' 		=> $contentData['content_page'],
			'contentTitle' 		=> $contentData['title'],
			'contentKeywords' 	=> $contentData['keywords'],
			'contentPHP' 		=> $contentData['php_page'],
			'operation' 		=> $contentData['operation'],
			'contentClass' 		=> $contentData['content_class'],
		);

        if ($_TREART['contentPHP'] && file_exists('../'.$_TREART['contentPHP'])) {
			require_once($_TREART['contentPHP']);
        } elseif ($_TREART['contentPHP']) {
			require_once('func.writeLog.php');
			writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking PHP Content file '".$_TREART["contentPHP"]."'", "Missing file", $_TREART, basename(__FILE__),'E',0);
		}

		if ($_TREART['contentHTML'] && file_exists('../htmls/ajax/'.$_TREART['contentHTML'])) {
			$tmplEngine->assign('page_content', $_TREART['contentHTML']);
		} elseif ($_TREART['contentHTML']) {
			require_once('func.writeLog.php');
			writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking HTML Content file '".$_TREART["contentHTML"]."'", "Missing file", $_TREART, basename(__FILE__),'E',0);
		}

	   $tmplEngine->assign(
		    array(  'userdata'          => $_SESSION[SITE_NAME]['login_data'],
		            'loginMessage'   => $_SESSION[SITE_NAME]['login_error'],
		            '_ENGINE'			=> $_TREART,
		            'sqlExecuted'    => $sqlCount,
		            'generationTime' => $timeElapsed,
		    )
		);

		$tmplEngine->assign($_POST);

		$tmplEngine->display('ajax/'.$_TREART['contentHTML']);
    }

}
?>