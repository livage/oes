<?php
function stripslashes_arrays(&$array) {
    if ( is_array($array) ) {
        $keys = array_keys($array);
        foreach ( $keys as $key ) {
            if ( is_array($array[$key]) ) {
                stripslashes_arrays($array[$key]);
            }
            else {
                $array[$key] = stripslashes($array[$key]);
            }
        }
    }
}
?>