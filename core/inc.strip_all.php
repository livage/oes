<?php
require_once("func.stripslashes_arrays.php");
if (get_magic_quotes_gpc()) {
    stripslashes_arrays($_GET);
    stripslashes_arrays($_POST);
    stripslashes_arrays($_COOKIE);
#    stripslashes_arrays($_FILES);
#verify $_FILES gets escapes by MAGIC_QUOTES first
}
?>