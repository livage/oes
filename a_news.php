<?php
require_once('core/func.getListOptions.php');
require_once('core/func.getListOptions.php');
require_once('core/func.getSqlOptions.php');
require_once('core/func.mysqlPrepare.php');
require_once('core/func.mysqlDate.php');


$lists = array(
	
);

switch ($_TREART['operation']) {
	case 'I':
			$opString = 'Insert';
			$sql = 'SELECT	DATE_FORMAT(INFULL(DATE_ADD(MAX(date), INTERVAL 1 DAY), NOW()), \'%d/%m/%Y\') nextDate
					FROM 	dailyposts
					WHERE 	active = 1';
			$data = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			$message['dateDMY'] = $data['nextDate'];
		break;

	case 'U':
			$opString = 'Edit';
			$sql = 'SELECT id,text,title, summary,image,attachment, DATE_FORMAT(`date`, \'%d/%m/%Y \') AS `date`
					FROM	news 
					WHERE id = '.intval($_GET['id']);
					
			$new = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			if ($new['image']) {
				$new['photoSmall'] = UPLOAD_PATH.'NEW_'.$new['id'].'_0_'.$new['image'];
				//NEW_42_0_Ford-Excursion.jpg
				$new['photoBig'] = UPLOAD_PATH.'NEW_'.$id.'_BIG_'.$new['image'];
				$new['photoBig'] = UPLOAD_PATH.'NEW_'.$id.'_ORG_'.$new['image'];
			}
			if ($new['attachment']) {
				$new['attachSmall'] = UPLOAD_PATH.'ATT_'.$new['id'].'_0_'.$new['attachment'];
				//NEW_42_0_Ford-Excursion.jpg
				$new['attachBig'] = UPLOAD_PATH.'ATT_'.$id.'_BIG_'.$new['attachment'];
				$new['attachBig'] = UPLOAD_PATH.'ATT_'.$id.'_ORG_'.$new['attachment'];
			}
			
		break;

	default:
			if ($_POST['button']) {
	foreach ($_POST as $field => $value) {
		if ($field != 'button') {
			$_SESSION[SITE_NAME]['filter'][$field] = $value;
		}
	}
    $actPage = 1;
} else {
    $actPage = intval($_GET['pg'])?intval($_GET['pg']):1;
}

$sqlFilter = ''; $sqlJoin = '';
$filter = &$_SESSION[SITE_NAME]['filter'];

if ($filter['filter_tst']) {
		$sqlFilter .= ' AND CONCAT(text, \' \', title , \' \', summary )LIKE \'%'.mysqlPrepare($filter['filter_tst']).'%\'';
		};
	
			$sql = 'SELECT	COUNT(*) counter
					FROM	news 			
					WHERE	1 '.$sqlFilter;
					
			$data = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			if ($results = $data['counter']) {
				require('inc.pagination.php');
				$sql = 'SELECT	*
						FROM	news 						
						WHERE	1 '.$sqlFilter.'
						ORDER BY insTS DESC
						LIMIT	'.(($actPage-1)*RESULT_PER_PAGE).", ".RESULT_PER_PAGE;
				
				$news = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			for ($i=0;$i<count($news);$i++){
					if ($news[$i]['image']) {
						//echo $news['photoSmall'];
						$news[$i]['photoSmall'] = UPLOAD_PATH.'NEW_'.$news[$i]['id'].'_0_'.$news[$i]['image'];
						$news[$i]['photoNormal'] = UPLOAD_PATH.'NEW_'.$news[$i]['id'].'_ORG_'.$news[$i]['image'];
						$news[$i]['photoBig'] = UPLOAD_PATH.'NEW_'.$news[$i]['id'].'_BIG_'.$news[$i]['image'];
					}
				}
			}
		break;
}

$tmplEngine->assign(
	array(
		'opString'		=> $opString,
		'new'			=> $new,
		'news'			=> $news,
		'filter'		=> $filter,
		'lists'			=> $lists,
		'results'  		=>  $results,
        'pages'      	=>  $pages,
        'pageList'      =>  $pageRange,
        'page'        	=>  $actPage,

	)
)
?>