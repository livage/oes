<?php
require('inc.init.php');
require('core/inc.config.php');

require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');

if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}
$fields = array(
	'STRING' => array(
		'customerName',
		'description',
		'market',
			
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
	),
	'DATETIME' => array(
	),
);
$mainTable = 'customers';

$nextPage = 'index.php?c=case_list';

switch ($op) {
      case 'I': // Inserimento
      
			if ($id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError)) {
			
				if ($_FILES['logo']['name']) {
					//echo ($_FILES['image']['name']); 	
					require('do.case.saveImage.php');
					
				}
			}
   case "U": // Aggiornamento
	    	doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
//	    	var_dump($_FILES['image']['name']); 
//	    		exit;
	    	if (!$sqlError) {
			
				if ($_FILES['logo']['name']) {
					require('do.case.removeImage.php');
					require('do.case.saveImage.php');
										
				}
	    	}
    	break;
	case "D": // delete the image
			require('do.case.removeImage.php');
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
	case 'S':
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
		break;
}
?>
<script language="javascript" type="text/javascript">
window.location = "<?php echo $nextPage?>";
</script>