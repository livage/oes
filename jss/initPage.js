
$(document).ready(function() {
//	$("._fDateTime").each(function() {
//		$(this).mask("99/99/9999 99:99", {
//		   placeholder:'_'
//		 });
//
//     	$(this).datetimepicker({
//			dateFormat: 'dd/mm/yy HH:MM',
//			firstDay: 1,
//			gotoCurrent:true,
//			showOn: 'button',
//			showAnim: 'fadeIn',
//			showOptions: {duration: 300 },
//			buttonImage: 'images/calendar.png',
//			buttonText: 'Click here to show the calendar',
//			buttonImageOnly: true
//     	});
//	});

	$("input[type=text], select, textarea").focus(function() {
		$(this).addClass("_fActive");
	})

	$("input[type=text], select, textarea").blur(function() {
		$(this).removeClass("_fActive");
	})


	$('._fDate').each(function() {
		$(this).mask('99/99/9999', {
			placeholder:'_'
		});

		$(this).datepicker({
			dateFormat: 'dd/mm/yy',
			firstDay: 1,
			gotoCurrent:true,
			showOn: 'button',
			showAnim: 'fadeIn',
			showOptions: {duration: 300 },
			buttonImage: 'images/calendar.png',
			buttonText: 'Click here to show the calendar',
			buttonImageOnly: true,
			dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
			dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			monthNames: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
			monthNamesShort: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
	  	});
	});
})