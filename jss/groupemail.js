$(document).ready(function(){
	$("textarea").css("overflow", "hidden");
	$("textarea").keyup(function() {
		var s = parseInt($(this).attr("scrollHeight"));
		var h = parseInt($(this).css("height"));
		var lh = parseInt($(this).css("line-height"));
		if(s > h) {
			$(this).css("height", s+'px');
		}
	})
});

function sendMessage(pMode) {
	var go = false;
	var post;
	if (pMode) {
		if (!$("[name=message]").val()) {
			alert('A message must be specified');
			return false;
		}
		if (!$("[name=subject]").val()) {
			if (!confirm('No subject has been specified. Do you want to continue anyway?')) {
				return false;
			}
		}
	}
	switch (pMode) {
		case 'test':
				if (!$("[name=testEmail]").val()) {
					alert('An e-mail address must be specified');
					return false;
				} else {
					go = true;
					post = 'do.groupemails.php?op=T&id='+$("[name=id]").val();
				}
			break;
		case 'final':
				go = confirm('Do you really want to send the message to all believers?');
				post = 'do.groupemails.php?op=S&id='+$("[name=id]").val();
			break;
	}

	if (go) {
		$("#massiveEmail").attr("action", post);
		$("#massiveEmail").submit();
	}

	return true;
}