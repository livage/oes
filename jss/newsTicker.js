$(document).ready(function() {
	newsTicker(".news_details");
})

function newsTicker(pSelector) {
	if ($(pSelector+' > a').length > 1) {
		var ticker = function() {
			setTimeout(function() {
				var h = $(pSelector+' > a').eq(0).height();
				$(pSelector+' > a').eq(0).animate( {marginTop: -h}, 1000, 'linear', function() {
					$(this).detach();
					$(this).removeAttr('style');
					$(pSelector).append($(this));
				});
				ticker();
			}, 6000);
		};
		ticker();
	}
}