function unlinkImage(pId) {

	var pType = "pictureImage";
	var picImage =pId;
		
	if (confirm('Are you sure you want to remove this picture?')){

		$('.immagine_presente').hide()
		$.ajax({
			type: "POST",
			url: "ajax.deleteImage.php?picImage="+picImage+"&op="+pType,
			success: function(result){
				if (result=='OK') {
					$('.immagine_presente').remove();
					
				} else {
				
				}
			},
			error: function () {
				alert('Si sono verificati errore in fase di cancellazione');
		
			}
		});
	}
}