function initTabs(pContainer) {
	// Check if subtab exists
	$('#'+pContainer+' > ._tabContent').each(function(i) {
		if (i) {
			$('#'+pContainer+' > ._tabContent').eq(i).hide();
			$('#'+pContainer+' > ._pageTabContainer > ._pageTab').eq(i).addClass('_pageTabUnselected');
		} else {
			$('#'+pContainer+' > ._pageTabContainer > ._pageTab').eq(i).addClass('_pageTabSelected');
		}

	});

	$('#'+pContainer+' > ._pageTabContainer > ._pageTab').click(function(){
		idxNew = $('#'+pContainer+' > ._pageTabContainer > ._pageTab').index($(this));
		idxAct = $('#'+pContainer+' > ._tabContent').index($('#'+pContainer+' > ._tabContent:visible'));

		if (idxNew != idxAct) {
			$('#'+pContainer+' > ._pageTabContainer > ._pageTab').eq(idxAct).toggleClass('_pageTabUnselected');
			$('#'+pContainer+' > ._pageTabContainer > ._pageTab').eq(idxAct).toggleClass('_pageTabSelected');
			$('#'+pContainer+' > ._pageTabContainer > ._pageTab').eq(idxNew).toggleClass('_pageTabUnselected');
			$('#'+pContainer+' > ._pageTabContainer > ._pageTab').eq(idxNew).toggleClass('_pageTabSelected');
			$('#'+pContainer+' > ._tabContent').eq(idxAct).hide()
			$('#'+pContainer+' > ._tabContent').eq(idxNew).show();
		}
	});
};

$(document).ready(function(){
	$('._tabMaster').each(function(){
		initTabs($(this).attr("id"));
	});
})