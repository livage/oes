<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');

$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
$id = intval($_GET['id']);

$fields = array(
	'STRING' => array(
		'name',
		'username',
		'user_password',
		'email',
		'profile',
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
	),
	'DATETIME' => array(
		'registrationExpire',
	),
);
$mailTable = '_users';

if ($_POST['user_password'] == '') {
	unset($_POST['user_password']);
}

$nextPage = 'index.php';

switch ($op) {
    case 'I': // Inserimento
			$id = doOperation($connection, 'I', $mailTable, $fields, $_POST, $sqlError);
    	break;
    case "U": // Aggiornamento
	    	doOperation($connection, 'U', $mailTable, $fields, $_POST, $sqlError, $id);
    	break;
	case "D": // delete the image
			doOperation($connection, 'D', $mailTable, $fields, $_POST, $sqlError, $id);
        break;
	case 'S':
			doOperation($connection, 'S', $mailTable, $fields, $_POST, $sqlError, $id);
		break;
}
?>
<script language="javascript" type="text/javascript">
window.location = "<?php echo $nextPage?>";
</script>