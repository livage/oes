<?php
$pages = ceil($results/RESULT_PER_PAGE);
$pageMinus = max(1, floor($actPage-(PAGE_LIST-1) / 2));
$pagePlus = min($pages, $pageMinus+PAGE_LIST-1);

$pagination = array(
	'results'		=>	$results,
	'page' 			=>	$actPage,
	'pages' 		=>	$pages,
	'pageMinus'		=>	$pageMinus,
	'pagePlus'		=>	$pagePlus,
	'pageRange'		=>	range($pageMinus, $pagePlus),
);
$tmplEngine->assign($pagination);
?>