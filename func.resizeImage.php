<?php
function storeImage($rowId,$imageInfo,$imgType,$sizes=array()) {
// Take the file info data
	$arFileData = pathinfo($imageInfo['name']);


	$fileName = $arFileData['filename'];
	$fileExt = $arFileData['extension'];
	if (is_array($sizes)) {

		foreach ($sizes as $prefix => $dims) {

			// Set File name
			$newFileName = $imgType.'_'.$rowId.'_'.$prefix.'_'.$imageInfo['name'];

			// Set max dimensions
			list($maxX, $maxY) = explode('x', $dims);

			// Get image sizes
			list($sizeX, $sizeY) = getimagesize($imageInfo['tmp_name']);

			switch (strtolower($fileExt))
			{
				case "png":
						$createFromFile = "imagecreatefrompng";
						$writeToDisk = "imagepng";
						break;
				case "jpg":
						$createFromFile = "imagecreatefromjpeg";
						$writeToDisk = "imagejpeg";
					break;
				case "gif":
						$createFromFile = "imagecreatefromgif";
						$writeToDisk = "imagegif";
					break;
			}


			// Create my image resource from the file
			$srcImg = $createFromFile($imageInfo['tmp_name']);

			// If greater size is less the the maximum
			$maxSize = max($maxX, $maxY);
			if ($maxSize == 0 || max($sizeX, $sizeY) <= $maxSize) { // nothing
				$newSizeX = $sizeX;
				$newSizeY = $sizeY;
			} else { // image to resize
				// compute the image aspect ratio
				$ratio = $sizeX/$sizeY;
				if ($ratio > 1) { // width > height
					$newSizeX = $maxSize;
					$newSizeY = $maxSize/$ratio;
				} else { // width <= height
					$newSizeX = $maxSize*$ratio;
					$newSizeY = $maxSize;
				}
			}

//			if ($maxX) $newX = $maxX;
//			if ($maxY) $newY = $maxY;

			$destImg = imagecreatetruecolor(round($newSizeX), round($newSizeY));

			// Resize the image
			imagecopyresampled($destImg,$srcImg,0, 0,0, 0,$newSizeX,$newSizeY,$sizeX,$sizeY);

		  	$writeToDisk($destImg, UPLOAD_PATH.$newFileName);

		}

	}

}



function resizeImage($originalImage,$toWidth,$toHeight)
{

    list($width, $height) = getimagesize($originalImage);
    $xscale=$width/$toWidth;
    $yscale=$height/$toHeight;

    if ($yscale>$xscale){
        $new_width = round($width * (1/$yscale));
        $new_height = round($height * (1/$yscale));
    }
    else {
        $new_width = round($width * (1/$xscale));
        $new_height = round($height * (1/$xscale));
    }
   
   
    $imageResized = imagecreatetruecolor($new_width, $new_height);
    $imageTmp     = imagecreatefromjpeg ($originalImage);
    imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

    return $imageResized;
	}
	?>