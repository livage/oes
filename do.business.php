<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');

if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}


$fields = array(
	'STRING' => array(
		'list_name',
		'list_value',
		'language',
		'list_label',				
	),
	'INT' => array(
	'list_id',
	),
	'FLOAT' => array(
	),
	'DATE' => array(
	
	),
	'DATETIME' => array(
	),
);

$mainTable = '_list_values';
$nextPage = 'index.php?c=business_list';

switch ($op) {
    
    case 'I': // Inserimento
    
      $id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError);
        
        break;
    case "U": // Aggiornamento
    
        doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
		
        break;
    
       
   case 'S':
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
    
}
?>
<script language="javascript" type="text/javascript">
    window.location = "<?php echo $nextPage?>";
</script>