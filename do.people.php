<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');

if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}

$fields = array(
	'STRING' => array(
		'name',
		'surname',
		'gender',
		'picture',
		'maritalStatus',
		'email',
		'country',
		'city',
		'pobox',
		'citizenship',
		'languages',
		'mobile',
		'occupation',
		'registrationString',
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
		'dateOfBirth',
		'weddingDate',
	),
	'DATETIME' => array(
		'registrationExpire',
	),
);
$mainTable = 'people';

switch ($op) {
    case 'I': // Inserimento
			if ($id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError)) {
				if ($_FILES['picture']['name']) {
					require('do.people.saveImage.php');
				}
			}
					
			if ($id = doOperation($connection, 'I', $table, $fields, $_POST, $sqlError)) {
				if ($_FILES['picture']['name']) {
					echo ($_FILES['picture']['name']); 	
					require('do.picture.saveImage.php');
					
					}
				}		
    		
    	break;
    case "U": // Aggiornamento
	    	doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
	    	if (!$sqlError) {
				if ($_FILES['picture']['name']) {
					require('do.people.removeImage.php');
					require('do.people.saveImage.php');
				}
	    	}
    	break;
	case "D": // delete the image
			require('do.people.removeImage.php');
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
	case 'S':
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
		break;
}
if ($skip != 'Y') {
?>
<script language="javascript" type="text/javascript">
window.location = "<?php echo $nextPage?>";
</script>
<?php
}
?>