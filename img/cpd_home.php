<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <title>Drug & Toxicology Information Service</title>
      <meta name="DaTIS Zimbabwe" content="Zimbabwe's Poison Control Centre. The Drug and Toxicology Information Service (DaTIS) is a unit within the School of Pharmacy at the University of Zimbabwe, College of Health Sciences which is also supported by the Ministry of Health and Child Welfare as a mission set up in 1979. The unit is also recognized by the World Health Organisation as a poisons information centre and is listed as such on the WHO website. DaTIS has traditionally provided consultation to mostly health care professionals and, to a smaller extent, the general public on drug information and toxicological/poisoning related issues.">
      <meta name="Professor D Tagwireyi" content="Director at DaTIS">
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Google Fonts  -->
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
      <!-- Library CSS -->
      <link rel="stylesheet" href="css/bootstrap.css">
      <link rel="stylesheet" href="css/bootstrap-theme.css">
      <link rel="stylesheet" href="css/fonts/font-awesome/css/font-awesome.css">
      <link rel="stylesheet" href="css/animations.css" media="screen">
      <link rel="stylesheet" href="css/superfish.css" media="screen">
      <link rel="stylesheet" href="css/prettyPhoto.css" media="screen">
      <!-- Theme CSS -->
      <link rel="stylesheet" href="css/style.css">
      <!-- Skin -->
      <link rel="stylesheet" href="css/colors/blue.css" class="colors">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="css/theme-responsive.css">
      <!-- Switcher CSS -->
      <link href="css/switcher.css" rel="stylesheet">
      <link href="css/spectrum.css" rel="stylesheet">
      <!-- Favicons -->
      <link rel="shortcut icon" href="img/ico/favicon.ico">
      <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
      <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72.png">
      <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114.png">
      <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144.png">
      <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
      <!--[if IE]>
      <link rel="stylesheet" href="css/ie.css">
      <![endif]-->
   </head>
   <body class="boxed blog single-post">
      <div class="wrap">
         <!-- Header Start -->
         <header id="header">
            <!-- Header Top Bar Start -->
            
            <!-- Header Top Bar End -->
            <!-- Main Header Start -->
            <div class="main-header">
               <div class="container">
                  <!-- TopNav Start -->
                  <div class="topnav navbar-header">
                     <a class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
                     <i class="fa fa-angle-down icon-current"></i>
                     </a> 
                  </div>
                  <!-- TopNav End -->
                  <!-- Logo Start -->
                  <div class="logo pull-left">
                     <h1>
                        <a href="index.html">
                        <img src="img/logo.png" alt="pixma" width="434" height="100">
                        </a>
                     </h1>
                  </div>
                  <!-- Logo End -->
                  <!-- Mobile Menu Start -->
                  <div class="mobile navbar-header">
                     <a class="navbar-toggle" data-toggle="collapse" href=".navbar-collapse">
                     <i class="fa fa-bars fa-2x"></i>
                     </a> 
                  </div>
                  <!-- Mobile Menu End -->
                  <!-- Menu Start -->
                  <nav class="collapse navbar-collapse menu">
                     <ul class="nav navbar-nav sf-menu">
                        <li>
                           <a id="current" href="index.html">
                           Home
                          
                           </a>
                        </li>
                        <li>
                           <a href="#" class="sf-with-ul">
                           About Us
                           
                           </a>      
                        </li>
                        <li>
                           <a href="#" class="sf-with-ul">
                           Our Services
                           
                           </a>
                        </li>
                        <li>
                           <a href="#" class="sf-with-ul">
                           CPD
                           <span class="sf-sub-indicator">
                           <i class="fa fa-angle-down "></i>
                           </span>
                           </a>
                           <ul>
                              <li><a href="portfolio-two.html" class="sf-with-ul">Sign In</a></li>
                              <li><a href="portfolio-three.html" class="sf-with-ul">Sign Up</a></li>
                           </ul>
                        </li>
                        <li>
                           <a href="#" class="sf-with-ul">
                           Blog
                           </a>
                        </li>
                        <li><a href="contact.html">
                            Contact
                            </a>
                         </li>
                     </ul>
                  </nav>
                  <!-- Menu End --> 
               </div>
            </div>
            <!-- Main Header End -->
         </header>
         <!-- Header End -->  
         <!-- Content Start -->
         <div id="main">
            <!-- Title, Breadcrumb Start-->
            <div class="breadcrumb-wrapper">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        
                     </div>
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <div class="breadcrumbs pull-right">
                           <ul>
                              <li>Welcome</li>
                              <li></li>
                              
                              
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Title, Breadcrumb End-->
            <!-- Main Content start-->
            <div class="content">
               <div class="container">
                  <div class="row">
                     <div class="posts-block col-lg-9 col-md-9 col-sm-8 col-xs-12 bottom-pad">
                        <article class="post hentry">
                           <embed width="845" height="500" src="modules/DATIS Reviews 4.pdf" type="application/pdf"></embed>
                           <header class="post-header">
                             <div class="well">
                           <a class="btn-small btn-color btn-pad" href="#">Download</a>
                           <div class="clearfix"></div>
                           </header>
                           
                           
                          
                        
                        <div class="col-md-6">
                    <div class="panel-group" id="accordion2">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-ico active">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
                                        100% Responsive Design
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-ico">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapse5">
                                        Pixel Perfect Design 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-ico">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapse6">
                                        Full Support
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        <!-- Reply Section End -->
                        <div class="col-md-6">
                    <div class="panel-group" id="accordion1">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-ico active">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapse1">
                                        100% Responsive Design
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-ico">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapse2">
                                        Pixel Perfect Design 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-ico">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapse3">
                                        Full Support
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     <div class="sidebar col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <!-- Search Widget Start -->
                        <div class="widget search-form">
                           <div class="input-group">
                              <input type="text" value="Search..." onfocus="if(this.value=='Search...')this.value='';" onblur="if(this.value=='')this.value='Search...';" class="search-input form-control">
                              <span class="input-group-btn">
                              <button type="submit" class="subscribe-btn btn"><i class="fa fa-search"></i></button>
                              </span>
                           </div>
                           <!-- /input-group -->
                        </div>
                        <!-- Search Widget End -->
                       
                       <!-- Category Widget Start -->
                        <div class="widget category">
                           <h3 class="title">My Account</h3>
                           <ul class="category-list slide">
                              <li><a href="#">CE Tracker</a></li>
                              <li><a href="#">Edit my Information</a></li>
                              <li><a href="#">Change Password</a></li>
                           </ul>
                        </div>
                        <!-- Category Widget End -->
                       
                        <!-- Tab Start -->
                        <div class="widget tabs">
                           <div id="horizontal-tabs">
                              <ul class="tabs">
                                 <li id="tab1" class="current">Volume 1</li>
                                 <li id="tab2">Volume 2</li>
                                 <li id="tab3">Volume 3 </li>
                              </ul>
                              <div class="contents">
                                 <div class="tabscontent" id="content1" style="display: block;">
                                    <ul class="posts">
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#"> Validation and its importance in BP and Glucose Measurements</a></p>
                                          <span class="color">27 July 2013</span>
                                       </li>
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Should we be concerned about Herbal
Medicines?</a></p>
                                          <span class="color">30 July 2013</span>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="tabscontent" id="content2">
                                    <ul class="posts">
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          <span class="color">27 July 2013</span>
                                       </li>
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          <span class="color">30 July 2013</span>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="tabscontent" id="content3">
                                    <ul class="posts">
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          by <span class="color">wptuts+</span>
                                       </li>
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          by <span class="color">wptuts+</span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Tab End -->
                        <!-- Testimonials Widget Start -->
                        
                        
                        <!-- Ads Widget Start -->
                        <div class="widget ads">
                           <h3 class="title">The DaTIS Bulletin</h3>
                           <div class="ads-img row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <img class="img-thumbnail" alt="" src="img/Datis_bulletin2.fw.png">
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <img class="img-thumbnail" alt="" src="img/Datis_bulletin.fw.png">
                              </div>
                           </div>
                           <div class="ads-img row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <img class="img-thumbnail" alt="" src="img/Datis_bulletin3.fw.png">
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <img class="img-thumbnail" alt="" src="img/Datis_bulletin4.fw.png">
                              </div>
                           </div>
                        </div>
                        <!-- Ads Widget End -->
                     </div>
                     <!-- Sidebar End --> 
                  </div>
               </div>
            </div>
            <!-- Main Content end-->
         </div>
         <!-- Content End -->
         <!-- Footer Start -->
         <footer id="footer">
            <!-- Footer Top Start -->
            <div class="footer-top">
               <div class="container">
                  <div class="row">
                     <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-one">
                        <h3>About DaTIS</h3>
                        <p> 
                           The Drug and Toxicology Information Service (DaTIS) is a unit within the School of Pharmacy at the University of Zimbabwe, College of Health Sciences which is also supported by the Ministry of Health and Child Welfare as a mission set up in 1979.
                        </p>
                     </section>
                     <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-two">
                        <h3>Twitter Stream</h3>
                        <ul id="tweets">
                        </ul>
                     </section>
                     <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-three">
                        <h3>Contact Us</h3>
                        <ul class="contact-us">
                           <li>
                              <i class="fa fa-map-marker"></i>
                              <p> 
                                 <strong class="contact-pad">Address:</strong> 
                              </p>
                           </li>
                           <li>
                              <i class="fa fa-phone"></i>
                              <p><strong>Phone:</strong> +263-4-707707, +263-4-791631</p>
                           </li>
                           <li>
                              <i class="fa fa-envelope"></i>
                           </li>
                        </ul>
                     </section>
                  </div>
               </div>
            </div>
            <!-- Footer Top End --> 
            <!-- Footer Bottom Start -->
            <div class="footer-bottom">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 "> &copy; Copyright 2016 by <a href="http://www.competus.co.zw">Competus</a>. All Rights Reserved. </div>
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 ">
                        <ul class="social social-icons-footer-bottom">
                           <li class="facebook"><a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                           <li class="twitter"><a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                           
                           <li class="linkedin"><a href="#" data-toggle="tooltip" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                           <li class="rss"><a href="#" data-toggle="tooltip" title="Rss"><i class="fa fa-rss"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Footer Bottom End --> 
         </footer>
         <!-- Scroll To Top --> 
         <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
      </div>
      <!-- Wrap End -->
      <section id="style-switcher">
        <h2>Style Switcher <a href="#"><i class="fa fa-cog"></i></a></h2>
        <div>
           <h3>Predefined Colors</h3>
           <ul class="colors">
              <li><a title="Blue" class="blue" href="#"></a></li>
              <li><a title="Green" class="green" href="#"></a></li>
              <li><a title="Orange" class="orange" href="#"></a></li>
              <li><a title="Purple" class="purple" href="#"></a></li>
              <li><a title="Red" class="red" href="#"></a></li>
              <li><a title="Magenta" class="magenta" href="#"></a></li>
              <li><a title="Brown" class="brown" href="#"></a></li>
              <li><a title="Gray" class="gray" href="#"></a></li>
              <li><a title="Golden" class="golden" href="#"></a></li>
              <li><a title="Teal" class="teal" href="#"></a></li>
           </ul>
           <h3>Layout Style</h3>
           <div class="layout-style">
              <select id="layout-style">
                 <option value="1">Wide</option>
                 <option value="2">Boxed</option>
              </select>
           </div>
           <h3>Header Color</h3>
           <div class="header-color">
              <input type='text' class="header-bg" />
           </div>
           <h3>Footer Top Color</h3>
           <div class="header-color">
              <input type='text' class="footer-bg" />
           </div>
           <h3>Footer Bottom Color</h3>
           <div class="header-color">
              <input type='text' class="footer-bottom" />
           </div>
           <h3>Background Image</h3>
           <ul id="bg" class="colors bg">
              <li><a class="bg1" href="#"></a></li>
              <li><a class="bg2" href="#"></a></li>
              <li><a class="bg3" href="#"></a></li>
              <li><a class="bg4" href="#"></a></li>
              <li><a class="bg5" href="#"></a></li>
              <li><a class="bg6" href="#"></a></li>
              <li><a class="bg7" href="#"></a></li>
              <li><a class="bg8" href="#"></a></li>
              <li><a class="bg9" href="#"></a></li>
              <li><a class="bg10" href="#"></a></li>
           </ul>
        </div>
     </section>
      
      <!-- The Scripts -->
      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.js"></script>
      <script src="js/jquery.parallax.js"></script>
      <script src="js/jquery.wait.js"></script> 
      <script src="js/modernizr-2.6.2.min.js"></script> 
      <script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
      <script src="js/jquery.nivo.slider.pack.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/superfish.js"></script>
      <script src="js/tweetMachine.js"></script>
      <script src="js/tytabs.js"></script>
      <script src="js/jquery.sticky.js"></script>
      <script src="js/jflickrfeed.js"></script>
      <script src="js/imagesloaded.pkgd.min.js"></script>
      <script src="js/waypoints.min.js"></script>
      <script src="js/jquery.gmap.min.js"></script>
      <script src="js/spectrum.js"></script>
      <script src="js/switcher.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>