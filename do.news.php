<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');

if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}
$fields = array(
	'STRING' => array(
		'title',
		'text',
		'summary',
			
	),
	'INT' => array(
	),
	'FLOAT' => array(
	),
	'DATE' => array(
		'date',
	),
	'DATETIME' => array(
	),
);
$mainTable = 'news';

$nextPage = 'index.php?c=news_list';

switch ($op) {
      case 'I': // Inserimento
      
			if ($id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError)) {
		
				if ($_FILES['image']['name']) {
					require('do.news.saveImage.php');
					
				}
				if ($_FILES['attachment']['name']) {
					require('do.news.saveAttach.php');
					
				}
			}
			break;
   case "U": // Aggiornamento
	    	doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
			
	    	if (!$sqlError) {
				if ($_FILES['image']['name']) {
					require('do.news.removeImage.php');
					require('do.news.saveImage.php');
				}
				if ($_FILES['attachment']['name']) {
					require('do.news.removeAttach.php');
					require('do.news.saveAttach.php');
				}
	    	}
    	break;
	case "D": // delete the image
			require('do.newsManagement.removeImage.php');
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
	case 'S':
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
		break;
}
?>
<script language="javascript" type="text/javascript">
window.location = "<?php echo $nextPage?>";
</script>